package main

import (
	"cmd/pkg/commands"
)

func main() {
	if err := commands.MainCmd.Execute(); err != nil {
		panic(err)
	}
}
