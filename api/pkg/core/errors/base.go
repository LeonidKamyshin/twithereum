package errors

import (
	"fmt"
	"strings"
)

type CodedError interface {
	error

	HTTPCode() int
	CharCode() string
	Description() string
	Details() map[string]any

	WithDescription(msg string, args ...any) CodedError
	WithCustomDetail(name string, value any) CodedError
}

type genericCodedError struct {
	CodedError

	httpCode int
	charCode string
	msg      string
	details  map[string]any
}

func concat(s ...string) string {
	return strings.Join(s, "")
}

func (e *genericCodedError) Error() string {
	return concat(e.charCode, ": ", e.msg)
}

func (e *genericCodedError) Description() string {
	return e.msg
}

func (e *genericCodedError) Unwrap() error {
	return e.CodedError
}

func (e *genericCodedError) HTTPCode() int {
	return e.httpCode
}

func (e *genericCodedError) CharCode() string {
	return e.charCode
}

func (e *genericCodedError) Details() map[string]any {
	return e.details
}

func (e *genericCodedError) WithDescription(msg string, args ...any) CodedError {
	return &genericCodedError{
		httpCode: e.httpCode,
		charCode: e.charCode,
		msg:      fmt.Sprintf(msg, args...),
		details:  e.details,
	}
}

func (e *genericCodedError) WithCustomDetail(name string, value any) CodedError {
	details := make(map[string]any)
	for k, v := range e.details {
		details[k] = v
	}
	details[name] = value
	return &genericCodedError{
		httpCode: e.httpCode,
		charCode: e.charCode,
		msg:      e.msg,
		details:  details,
	}
}
