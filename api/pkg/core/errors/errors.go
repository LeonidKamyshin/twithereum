package errors

import (
	"net/http"
)

type userError struct {
	genericCodedError
}

func (e *userError) WithAddress(address string) *userError {
	err := e.WithCustomDetail("address", address)
	return &userError{*err.(*genericCodedError)}
}

var (
	ErrDatabase = &genericCodedError{
		charCode: "DATABASE_ERROR",
		msg:      "Database error.",
		httpCode: http.StatusInternalServerError,
	}
	ErrNotFound = &genericCodedError{
		charCode: "NOT_FOUND",
		msg:      "Not found.",
		httpCode: http.StatusNotFound,
	}
	ErrInvalidRequest = &genericCodedError{
		charCode: "INVALID_REQUEST",
		msg:      "Invalid request.",
		httpCode: http.StatusBadRequest,
	}
	ErrLogic = &genericCodedError{
		charCode: "LOGICAL_ERROR",
		msg:      "Internal server error.",
		httpCode: http.StatusInternalServerError,
	}
	ErrUserNotFound = &userError{
		genericCodedError{
			charCode: "USER_NOT_FOUND",
			msg:      "User not found.",
			httpCode: http.StatusUnprocessableEntity,
		},
	}
	ErrValidation = &genericCodedError{
		charCode: "VALIDATION_ERROR",
		msg:      "Validation error.",
		httpCode: http.StatusUnprocessableEntity,
	}
)
