package config

import (
	"context"
	"fmt"
	"os"

	"github.com/heetch/confita"
	"github.com/heetch/confita/backend/file"
)

const (
	defaultConfigPath = "cmd/config/common.yaml"
)

type Postgres struct {
	Host            string `config:"host,required"`
	Port            uint16 `config:"port,required"`
	Password        string `config:"password,required"`
	User            string `config:"user,required"`
	Database        string `config:"database,required"`
	MaxOpenConn     int32  `config:"maxOpenConn,required" yaml:"maxOpenConn"`
	MinOpenConn     int32  `config:"minOpenConn,required" yaml:"minOpenConn"`
	MaxConnLifetime string `config:"maxConnLifetime,required" yaml:"maxConnLifetime"`
	MaxConnIdleTime string `config:"maxConnIdleTime,required" yaml:"maxConnIdleTime"`
}

func (p *Postgres) GetUrl() string {
	const template = "postgres://%s:%s@%s:%d/%s"
	return fmt.Sprintf(template, p.User, p.Password, p.Host, p.Port, p.Database)
}

func (p *Postgres) GetDSN() string {
	const template = "host=%s " +
		"port=%d " +
		"password=%s " +
		"user=%s " +
		"database=%s " +
		"pool_max_conns=%d " +
		"pool_min_conns=%d " +
		"pool_max_conn_lifetime=%s " +
		"pool_max_conn_idle_time=%s"
	return fmt.Sprintf(
		template,
		p.Host,
		p.Port,
		p.Password,
		p.User,
		p.Database,
		p.MaxOpenConn,
		p.MinOpenConn,
		p.MaxConnLifetime,
		p.MaxConnIdleTime,
	)
}

type SwaggerUI struct {
	Enabled  bool   `yaml:"enabled"`
	SpecsDir string `yaml:"specsDir"`
}

type Config struct {
	Postgres  Postgres  `config:"postgres,required"`
	SwaggerUI SwaggerUI `yaml:"swaggerUI"`
}

func GetConfig() Config {
	configPath := defaultConfigPath

	if os.Getenv("TW_CONFIG") != "" {
		configPath = os.Getenv("TW_CONFIG")
	}

	loader := confita.NewLoader(
		file.NewBackend(configPath),
	)

	cfg := &Config{}

	err := loader.Load(context.Background(), cfg)

	if err != nil {
		panic(err)
	}
	return *cfg
}
