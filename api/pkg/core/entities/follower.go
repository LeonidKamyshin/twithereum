package entities

import (
	"cmd/pkg/core/errors"
	"net/http"
	"strconv"
)

type Follower struct {
	UserInfo
}

type GetFollowersParams struct {
	Address string
	Limit   int64
	FirstID *int64
}

func (g *GetFollowersParams) Validate(r *http.Request) error {
	query := r.URL.Query()
	if !query.Has("address") {
		return errors.ErrInvalidRequest.WithDescription("query: address is required")
	}

	limit, err := strconv.ParseInt(query.Get("limit"), 10, 64)
	if err != nil {
		return errors.ErrInvalidRequest.WithDescription("query: limit must be int64")
	}
	if limit <= 0 {
		return errors.ErrValidation.WithDescription("query: limit must be positive")
	}

	if query.Has("first_id") {
		firstID, err := strconv.ParseInt(query.Get("first_id"), 10, 64)
		if err != nil {
			return errors.ErrInvalidRequest.WithDescription("query: first_id must be int64")
		}
		if firstID <= 0 {
			return errors.ErrValidation.WithDescription("query: first_id must be positive")
		}
		g.FirstID = &firstID
	}

	g.Address = query.Get("address")
	g.Limit = limit

	return nil
}
