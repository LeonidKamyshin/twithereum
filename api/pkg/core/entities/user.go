package entities

import (
	"cmd/pkg/core/errors"
	"cmd/pkg/utils"
	"net/http"
	"strconv"
)

type User struct {
	Address        string `json:"address"`
	Username       string `json:"username"`
	Followers      int64  `json:"followers"`
	Followed       int64  `json:"followed"`
	FollowedStatus bool   `json:"followed_status"`
}

type UserInfo struct {
	ID       int64  `json:"id"`
	Address  string `json:"address"`
	Username string `json:"username"`
}

type GetUserParams struct {
	Address       string
	SenderAddress *string
}

func (g *GetUserParams) Validate(r *http.Request) error {
	query := r.URL.Query()
	if !query.Has("address") {
		return errors.ErrInvalidRequest.WithDescription("query: address is required")
	}

	if query.Has("sender_address") {
		g.SenderAddress = utils.PointerTo(query.Get("sender_address"))
	}

	g.Address = query.Get("address")
	return nil
}

type GetUsersLikeParams struct {
	Limit    int64
	Address  *string
	Username *string
	FirstID  *int64
}

func (g *GetUsersLikeParams) Validate(r *http.Request) error {
	query := r.URL.Query()

	if !query.Has("limit") {
		return errors.ErrInvalidRequest.WithDescription("query: limit is required")
	}

	limit, err := strconv.ParseInt(query.Get("limit"), 10, 64)
	if err != nil {
		return errors.ErrInvalidRequest.WithDescription("query: limit must be int64")
	}
	if limit <= 0 {
		return errors.ErrValidation.WithDescription("query: limit must be positive")
	}

	if query.Has("address") {
		g.Address = utils.PointerTo(query.Get("address"))
	}

	if query.Has("username") {
		g.Username = utils.PointerTo(query.Get("username"))
	}

	if query.Has("first_id") {
		firstID, err := strconv.ParseInt(query.Get("first_id"), 10, 64)
		if err != nil {
			return errors.ErrInvalidRequest.WithDescription("query: first_id must be int64")
		}
		if firstID <= 0 {
			return errors.ErrValidation.WithDescription("query: first_id must be positive")
		}
		g.FirstID = &firstID
	}

	g.Limit = limit

	return nil
}
