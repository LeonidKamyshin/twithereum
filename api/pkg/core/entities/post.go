package entities

import (
	"cmd/pkg/core/errors"
	"cmd/pkg/utils"
	"fmt"
	"net/http"
	"strconv"
	"time"
)

type Post struct {
	Author    string    `json:"author"`
	Username  string    `json:"username"`
	ID        int64     `json:"id"`
	Data      []byte    `json:"data"`
	CreatedAt time.Time `json:"created_at"`
}

type GetPostsParams struct {
	Address       string
	Limit         int64
	CreatedBefore *time.Time
	CreatedAfter  *time.Time
	StartPostID   *int64
}

func (g *GetPostsParams) Validate(r *http.Request) error {
	query := r.URL.Query()

	if !query.Has("limit") {
		return errors.ErrInvalidRequest.WithDescription("query: limit is required")
	}

	limit, err := strconv.ParseInt(query.Get("limit"), 10, 64)
	if err != nil {
		return errors.ErrInvalidRequest.WithDescription("query: limit must be int64")
	}
	if limit <= 0 {
		return errors.ErrValidation.WithDescription("query: limit must be positive")
	}

	if !query.Has("address") {
		return errors.ErrInvalidRequest.WithDescription("query: address is required")
	}

	if query.Has("created_before") {
		createdBefore, err := utils.ParseDateTime(query.Get("created_before"))
		if err != nil {
			return errors.ErrInvalidRequest.WithDescription(
				fmt.Sprintf("query: created_before must be datetime in format: %s", utils.DateFormat),
			)
		}
		g.CreatedBefore = &createdBefore
	}

	if query.Has("created_after") {
		createdAfter, err := utils.ParseDateTime(query.Get("created_after"))
		if err != nil {
			return errors.ErrInvalidRequest.WithDescription(
				fmt.Sprintf("query: created_after must be datetime in format: %s", utils.DateFormat),
			)
		}
		g.CreatedAfter = &createdAfter
	}

	if g.CreatedAfter != nil && g.CreatedBefore != nil && g.CreatedBefore.Before(*g.CreatedAfter) {
		return errors.ErrValidation.WithDescription("created_before must be greater than created_after")
	}

	if query.Has("start_post_id") {
		startPostId, err := strconv.ParseInt(query.Get("start_post_id"), 10, 64)
		if err != nil {
			return errors.ErrInvalidRequest.WithDescription("query: start_post_id must be int64")
		}
		if startPostId < 0 {
			return errors.ErrValidation.WithDescription("query: start_post_id must be non negative")
		}
		g.StartPostID = &startPostId
	}

	g.Address = query.Get("address")
	g.Limit = limit

	return nil
}

type GetAllPostsParams struct {
	Limit         int64
	CreatedBefore *time.Time
	CreatedAfter  *time.Time
	StartPostID   *int64
}

func (g *GetAllPostsParams) Validate(r *http.Request) error {
	query := r.URL.Query()

	if !query.Has("limit") {
		return errors.ErrInvalidRequest.WithDescription("query: limit is required")
	}

	limit, err := strconv.ParseInt(query.Get("limit"), 10, 64)
	if err != nil {
		return errors.ErrInvalidRequest.WithDescription("query: limit must be int64")
	}
	if limit < 0 {
		return errors.ErrValidation.WithDescription("query: limit must be non negative")
	}

	if query.Has("created_before") {
		createdBefore, err := utils.ParseDateTime(query.Get("created_before"))
		if err != nil {
			return errors.ErrInvalidRequest.WithDescription(
				fmt.Sprintf("query: created_before must be datetime in format: %s", utils.DateFormat),
			)
		}
		g.CreatedBefore = &createdBefore
	}

	if query.Has("created_after") {
		createdAfter, err := utils.ParseDateTime(query.Get("created_after"))
		if err != nil {
			return errors.ErrInvalidRequest.WithDescription(
				fmt.Sprintf("query: created_after must be datetime in format: %s", utils.DateFormat),
			)
		}
		g.CreatedAfter = &createdAfter
	}

	if g.CreatedAfter != nil && g.CreatedBefore != nil && g.CreatedBefore.Before(*g.CreatedAfter) {
		return errors.ErrValidation.WithDescription("created_before must be greater than created_after")
	}

	if query.Has("start_post_id") {
		startPostId, err := strconv.ParseInt(query.Get("start_post_id"), 10, 64)
		if err != nil {
			return errors.ErrInvalidRequest.WithDescription("query: start_post_id must be int64")
		}
		if startPostId < 0 {
			return errors.ErrValidation.WithDescription("query: start_post_id must be non negative")
		}
		g.StartPostID = &startPostId
	}

	g.Limit = limit

	return nil
}
