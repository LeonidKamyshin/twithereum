package entities

import (
	"net/http"
)

type Validatable interface {
	Validate(r *http.Request) error
}

func ParseAndValidateRequest(r *http.Request, destPtr Validatable) error {
	if err := destPtr.Validate(r); err != nil {
		return err
	}

	return nil
}
