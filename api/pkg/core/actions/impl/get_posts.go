package impl

import (
	"cmd/pkg/core/entities"
	"cmd/pkg/core/errors"

	"github.com/xfxdev/xlog"
)

const sqlGetPosts = `
	WITH posts AS (
		SELECT p.id,
			   p.address,
			   u.username,
			   p.p_data,
			   p.created_at
		FROM tw.posts p
    	INNER JOIN tw.users u ON u.address = p.address
		WHERE ($2::timestamptz IS NULL OR p.created_at < $2::timestamptz)
			AND ($3::timestamptz IS NULL OR p.created_at > $3::timestamptz)
			AND ($4::bigint IS NULL OR p.id <= $4::bigint)
	), total_count AS (
		SELECT count(id) AS count FROM posts
	), posts_limited AS (
		SELECT * FROM posts ORDER BY id DESC LIMIT $1
	)
	SELECT
		id,
		address,
		username,
		p_data,
		created_at,
		total_count.count
	FROM posts_limited
	FULL OUTER JOIN total_count ON true;
`

func (a *ActionAdapter) GetPosts(params entities.GetAllPostsParams) ([]*entities.Post, int64, error) {
	posts := make([]*entities.Post, 0)
	rows, err := a.storage.Conn().Query(a.ctx, sqlGetPosts,
		params.Limit,
		params.CreatedBefore,
		params.CreatedAfter,
		params.StartPostID,
	)
	if err != nil {
		xlog.Error(err)
		return nil, 0, errors.ErrDatabase
	}

	var totalCount int64
	for rows.Next() {
		post := entities.Post{}

		err = rows.Scan(nil, nil, nil, nil, nil, &totalCount)
		if err != nil {
			xlog.Error(err)
			return nil, 0, errors.ErrDatabase
		}
		if totalCount == 0 {
			return posts, totalCount, nil
		}

		err = rows.Scan(
			&post.ID,
			&post.Author,
			&post.Username,
			&post.Data,
			&post.CreatedAt,
			&totalCount,
		)
		if err != nil {
			xlog.Error(err)
			return nil, 0, errors.ErrDatabase
		}

		posts = append(posts, &post)
	}

	return posts, totalCount, nil
}
