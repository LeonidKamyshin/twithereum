package impl

import (
	"cmd/pkg/core/entities"
	"cmd/pkg/core/errors"

	"github.com/jackc/pgx/v5"
	"github.com/xfxdev/xlog"
)

const sqlGetUser = `
	WITH users AS (
		SELECT
			address,
			username,
			followers,
			followed
		FROM tw.users WHERE LOWER(address)=LOWER($1)
	), followed_info AS (
		SELECT EXISTS(
			SELECT 1
			FROM tw.followers
			WHERE $2::text IS NOT NULL
			  AND LOWER(followed_id) = LOWER($1)
			  AND LOWER(user_id) = LOWER($2::text)
			) AS is_followed
	)
	SELECT
		address,
		username,
		followers,
		followed,
		followed_info.is_followed
	FROM users
	LEFT JOIN followed_info ON true;
`

func (a *ActionAdapter) GetUser(params entities.GetUserParams) (*entities.User, error) {
	user := entities.User{}
	err := a.storage.Conn().QueryRow(a.ctx, sqlGetUser, params.Address, params.SenderAddress).Scan(
		&user.Address,
		&user.Username,
		&user.Followers,
		&user.Followed,
		&user.FollowedStatus,
	)
	if err == pgx.ErrNoRows {
		return nil, errors.ErrUserNotFound.WithAddress(params.Address)
	}
	if err != nil {
		xlog.Error(err)
		return nil, errors.ErrDatabase
	}

	return &user, nil
}
