package impl

import (
	"cmd/pkg/core/entities"
	"cmd/pkg/core/errors"

	"github.com/xfxdev/xlog"
)

const sqlGetUsersLike = `
	WITH users AS (
		SELECT u.address,
			   u.id,
			   u.username
		FROM tw.users u
		WHERE ($2::varchar IS NULL OR LOWER(u.address) LIKE '%'|| LOWER($2::varchar) ||'%')
			AND ($3::varchar IS NULL OR LOWER(u.username) LIKE '%'|| LOWER($3::varchar) ||'%')
			AND ($4::bigint IS NULL OR u.id <= $4::bigint)
	), total_count AS (
		SELECT count(id) AS count FROM users
	), users_limited AS (
		SELECT * FROM users ORDER BY id DESC LIMIT $1
	)
	SELECT
		id,
		address,
		username,
		total_count.count
	FROM users_limited
	FULL OUTER JOIN total_count ON true;
`

func (a *ActionAdapter) GetUsersLike(params entities.GetUsersLikeParams) ([]*entities.UserInfo, int64, error) {
	users := make([]*entities.UserInfo, 0)
	rows, err := a.storage.Conn().Query(a.ctx, sqlGetUsersLike,
		params.Limit,
		params.Address,
		params.Username,
		params.FirstID,
	)
	if err != nil {
		xlog.Error(err)
		return nil, 0, errors.ErrDatabase
	}

	var totalCount int64
	for rows.Next() {
		user := entities.UserInfo{}

		err = rows.Scan(nil, nil, nil, &totalCount)
		if err != nil {
			xlog.Error(err)
			return nil, 0, errors.ErrDatabase
		}
		if totalCount == 0 {
			return users, totalCount, nil
		}

		err = rows.Scan(
			&user.ID,
			&user.Address,
			&user.Username,
			&totalCount,
		)
		if err != nil {
			xlog.Error(err)
			return nil, 0, errors.ErrDatabase
		}

		users = append(users, &user)
	}

	return users, totalCount, nil
}
