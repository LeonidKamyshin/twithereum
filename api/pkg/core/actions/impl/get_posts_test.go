package impl

import (
	"testing"
	"time"

	"cmd/pkg/core/entities"
	"cmd/pkg/utils"

	"github.com/stretchr/testify/suite"
)

func TestGetPostsTestSuite(t *testing.T) {
	suite.Run(t, new(GetPostsSuite))
}

type GetPostsSuite struct {
	baseActionTestSuite

	user1 *entities.User
	user2 *entities.User

	post1 entities.Post
	post2 entities.Post
	post3 entities.Post
	post4 entities.Post
	post5 entities.Post
	post6 entities.Post
	post7 entities.Post
}

func (s *GetPostsSuite) SetupSuite() {
	s.baseActionTestSuite.SetupSuite()
}

func (s *GetPostsSuite) SetupTest() {
	s.baseActionTestSuite.SetupTest()

	s.user1 = &entities.User{
		Address:   "address",
		Username:  "username",
		Followers: 0,
		Followed:  0,
	}
	s.post1 = entities.Post{
		ID:        5,
		Author:    s.user1.Address,
		Data:      make([]byte, 0),
		CreatedAt: time.Date(2020, 01, 02, 12, 23, 34, 0, time.Local),
	}
	s.post2 = entities.Post{
		ID:        533,
		Author:    s.user1.Address,
		Data:      make([]byte, 0),
		CreatedAt: time.Date(2020, 01, 04, 12, 23, 34, 0, time.Local),
	}
	s.post3 = entities.Post{
		ID:        577,
		Author:    s.user1.Address,
		Data:      make([]byte, 0),
		CreatedAt: time.Date(2020, 01, 8, 12, 23, 34, 0, time.Local),
	}
	s.post4 = entities.Post{
		ID:        600,
		Author:    s.user1.Address,
		Data:      make([]byte, 0),
		CreatedAt: time.Date(2020, 01, 9, 12, 23, 34, 0, time.Local),
	}
	s.post5 = entities.Post{
		ID:        620,
		Author:    s.user1.Address,
		Data:      make([]byte, 0),
		CreatedAt: time.Date(2020, 01, 15, 12, 23, 34, 0, time.Local),
	}

	s.user2 = &entities.User{
		Address:   "address12311",
		Username:  "username133441",
		Followers: 0,
		Followed:  0,
	}
	s.post6 = entities.Post{
		ID:        678,
		Author:    s.user2.Address,
		Data:      make([]byte, 0),
		CreatedAt: time.Date(2020, 01, 16, 12, 23, 34, 0, time.Local),
	}
	s.post7 = entities.Post{
		ID:        900,
		Author:    s.user2.Address,
		Data:      make([]byte, 0),
		CreatedAt: time.Date(2020, 01, 18, 12, 23, 34, 0, time.Local),
	}

	s.factory.CreateUser(*s.user1)
	s.factory.CreateUser(*s.user2)
	s.factory.CreatePosts(s.post3, s.post5)
	s.factory.CreatePosts(s.post1, s.post2, s.post4, s.post6, s.post7)
}

func (s *GetPostsSuite) TestGetUserPosts_WithoutExtraConditions_LimitLessPostsCount() {
	params := entities.GetAllPostsParams{
		Limit: 5,
	}

	posts, totalCount, err := s.actions.GetPosts(params)
	expPosts := []entities.Post{s.post7, s.post6, s.post5, s.post4, s.post3}

	s.Require().NoError(err)
	s.Require().Equal(expPosts, utils.PointerArrayToSimple(posts))
	s.Require().Equal(int64(7), totalCount)
}

func (s *GetPostsSuite) TestGetUserPosts_WithoutExtraConditions_LimitMorePostsCount() {
	params := entities.GetAllPostsParams{
		Limit: 10,
	}

	posts, totalCount, err := s.actions.GetPosts(params)
	expPosts := []entities.Post{s.post7, s.post6, s.post5, s.post4, s.post3, s.post2, s.post1}

	s.Require().NoError(err)
	s.Require().Equal(expPosts, utils.PointerArrayToSimple(posts))
	s.Require().Equal(int64(7), totalCount)
}

func (s *GetPostsSuite) TestGetUserPosts_WithStartPostID() {
	params := entities.GetAllPostsParams{
		Limit:       2,
		StartPostID: &s.post4.ID,
	}

	posts, totalCount, err := s.actions.GetPosts(params)
	expPosts := []entities.Post{s.post4, s.post3}

	s.Require().NoError(err)
	s.Require().Equal(expPosts, utils.PointerArrayToSimple(posts))
	s.Require().Equal(int64(4), totalCount)
}

func (s *GetPostsSuite) TestGetUserPosts_WithStartPostID_EmptyArray() {
	params := entities.GetAllPostsParams{
		Limit:       10,
		StartPostID: utils.PointerTo(int64(1)),
	}

	posts, totalCount, err := s.actions.GetPosts(params)
	expPosts := make([]entities.Post, 0)

	s.Require().NoError(err)
	s.Require().Equal(expPosts, utils.PointerArrayToSimple(posts))
	s.Require().Equal(int64(0), totalCount)
}

func (s *GetPostsSuite) TestGetUserPosts_WithCreatedBefore() {
	params := entities.GetAllPostsParams{
		Limit:         10,
		StartPostID:   &s.post5.ID,
		CreatedBefore: &s.post4.CreatedAt,
	}

	posts, totalCount, err := s.actions.GetPosts(params)
	expPosts := []entities.Post{s.post3, s.post2, s.post1}

	s.Require().NoError(err)
	s.Require().Equal(expPosts, utils.PointerArrayToSimple(posts))
	s.Require().Equal(int64(3), totalCount)
}

func (s *GetPostsSuite) TestGetUserPosts_WithCreatedAfter() {
	params := entities.GetAllPostsParams{
		Limit:        10,
		StartPostID:  &s.post5.ID,
		CreatedAfter: &s.post4.CreatedAt,
	}

	posts, totalCount, err := s.actions.GetPosts(params)
	expPosts := []entities.Post{s.post5}

	s.Require().NoError(err)
	s.Require().Equal(expPosts, utils.PointerArrayToSimple(posts))
	s.Require().Equal(int64(1), totalCount)
}

func (s *GetPostsSuite) TestGetUserPosts_WithPeriod() {
	params := entities.GetAllPostsParams{
		Limit:         3,
		CreatedAfter:  &s.post1.CreatedAt,
		CreatedBefore: &s.post7.CreatedAt,
	}

	posts, totalCount, err := s.actions.GetPosts(params)
	expPosts := []entities.Post{s.post6, s.post5, s.post4}

	s.Require().NoError(err)
	s.Require().Equal(expPosts, utils.PointerArrayToSimple(posts))
	s.Require().Equal(int64(5), totalCount)
}
