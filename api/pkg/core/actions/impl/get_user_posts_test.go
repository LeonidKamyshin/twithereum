package impl

import (
	"testing"
	"time"

	"cmd/pkg/core/entities"
	"cmd/pkg/utils"

	"github.com/stretchr/testify/suite"
)

func TestGetUserPostsTestSuite(t *testing.T) {
	suite.Run(t, new(GetUserPostsSuite))
}

type GetUserPostsSuite struct {
	baseActionTestSuite

	user *entities.User

	post1 entities.Post
	post2 entities.Post
	post3 entities.Post
	post4 entities.Post
	post5 entities.Post
}

func (s *GetUserPostsSuite) SetupSuite() {
	s.baseActionTestSuite.SetupSuite()
}

func (s *GetUserPostsSuite) SetupTest() {
	s.baseActionTestSuite.SetupTest()

	s.user = &entities.User{
		Address:   "address",
		Username:  "username",
		Followers: 0,
		Followed:  0,
	}
	s.post1 = entities.Post{
		ID:        5,
		Author:    s.user.Address,
		Data:      make([]byte, 0),
		CreatedAt: time.Date(2020, 01, 02, 12, 23, 34, 0, time.Local),
	}
	s.post2 = entities.Post{
		ID:        533,
		Author:    s.user.Address,
		Data:      make([]byte, 0),
		CreatedAt: time.Date(2020, 01, 04, 12, 23, 34, 0, time.Local),
	}
	s.post3 = entities.Post{
		ID:        600,
		Author:    s.user.Address,
		Data:      make([]byte, 0),
		CreatedAt: time.Date(2020, 01, 8, 12, 23, 34, 0, time.Local),
	}
	s.post4 = entities.Post{
		ID:        620,
		Author:    s.user.Address,
		Data:      make([]byte, 0),
		CreatedAt: time.Date(2020, 01, 9, 12, 23, 34, 0, time.Local),
	}
	s.post5 = entities.Post{
		ID:        900,
		Author:    s.user.Address,
		Data:      make([]byte, 0),
		CreatedAt: time.Date(2020, 01, 15, 12, 23, 34, 0, time.Local),
	}

	user := entities.User{
		Address:   "address12311",
		Username:  "username133441",
		Followers: 0,
		Followed:  0,
	}
	post1 := entities.Post{
		ID:        577,
		Author:    user.Address,
		Data:      make([]byte, 0),
		CreatedAt: time.Date(2020, 01, 02, 12, 23, 34, 0, time.Local),
	}
	post2 := entities.Post{
		ID:        678,
		Author:    user.Address,
		Data:      make([]byte, 0),
		CreatedAt: time.Date(2020, 01, 10, 12, 23, 34, 0, time.Local),
	}

	s.factory.CreateUser(user)
	s.factory.CreatePosts(post1, post2)
	s.factory.CreateUser(*s.user)
	s.factory.CreatePosts(s.post1, s.post2, s.post3, s.post4, s.post5)
}

func (s *GetUserPostsSuite) TestGetUserPosts_WithoutExtraConditions_LimitLessPostsCount() {
	params := entities.GetPostsParams{
		Address: s.user.Address,
		Limit:   3,
	}

	posts, totalCount, err := s.actions.GetUserPosts(params)
	expPosts := []entities.Post{s.post5, s.post4, s.post3}

	s.Require().NoError(err)
	s.Require().Equal(expPosts, utils.PointerArrayToSimple(posts))
	s.Require().Equal(int64(5), totalCount)
}

func (s *GetUserPostsSuite) TestGetUserPosts_WithoutExtraConditions_LimitMorePostsCount() {
	params := entities.GetPostsParams{
		Address: s.user.Address,
		Limit:   10,
	}

	posts, totalCount, err := s.actions.GetUserPosts(params)
	expPosts := []entities.Post{s.post5, s.post4, s.post3, s.post2, s.post1}

	s.Require().NoError(err)
	s.Require().Equal(expPosts, utils.PointerArrayToSimple(posts))
	s.Require().Equal(int64(5), totalCount)
}

func (s *GetUserPostsSuite) TestGetUserPosts_WithStartPostID() {
	params := entities.GetPostsParams{
		Address:     s.user.Address,
		Limit:       10,
		StartPostID: &s.post3.ID,
	}

	posts, totalCount, err := s.actions.GetUserPosts(params)
	expPosts := []entities.Post{s.post3, s.post2, s.post1}

	s.Require().NoError(err)
	s.Require().Equal(expPosts, utils.PointerArrayToSimple(posts))
	s.Require().Equal(int64(3), totalCount)
}

func (s *GetUserPostsSuite) TestGetUserPosts_WithStartPostID_EmptyArray() {
	params := entities.GetPostsParams{
		Address:     s.user.Address,
		Limit:       10,
		StartPostID: utils.PointerTo(int64(1)),
	}

	posts, totalCount, err := s.actions.GetUserPosts(params)
	expPosts := make([]entities.Post, 0)

	s.Require().NoError(err)
	s.Require().Equal(expPosts, utils.PointerArrayToSimple(posts))
	s.Require().Equal(int64(0), totalCount)
}

func (s *GetUserPostsSuite) TestGetUserPosts_WithCreatedBefore() {
	params := entities.GetPostsParams{
		Address:       s.user.Address,
		Limit:         10,
		StartPostID:   &s.post5.ID,
		CreatedBefore: &s.post4.CreatedAt,
	}

	posts, totalCount, err := s.actions.GetUserPosts(params)
	expPosts := []entities.Post{s.post3, s.post2, s.post1}

	s.Require().NoError(err)
	s.Require().Equal(expPosts, utils.PointerArrayToSimple(posts))
	s.Require().Equal(int64(3), totalCount)
}

func (s *GetUserPostsSuite) TestGetUserPosts_WithCreatedAfter() {
	params := entities.GetPostsParams{
		Address:      s.user.Address,
		Limit:        10,
		StartPostID:  &s.post5.ID,
		CreatedAfter: &s.post4.CreatedAt,
	}

	posts, totalCount, err := s.actions.GetUserPosts(params)
	expPosts := []entities.Post{s.post5}

	s.Require().NoError(err)
	s.Require().Equal(expPosts, utils.PointerArrayToSimple(posts))
	s.Require().Equal(int64(1), totalCount)
}

func (s *GetUserPostsSuite) TestGetUserPosts_WithPeriod() {
	params := entities.GetPostsParams{
		Address:       s.user.Address,
		Limit:         1,
		StartPostID:   &s.post5.ID,
		CreatedAfter:  &s.post2.CreatedAt,
		CreatedBefore: &s.post5.CreatedAt,
	}

	posts, totalCount, err := s.actions.GetUserPosts(params)
	expPosts := []entities.Post{s.post4}

	s.Require().NoError(err)
	s.Require().Equal(expPosts, utils.PointerArrayToSimple(posts))
	s.Require().Equal(int64(2), totalCount)
}
