package impl

import (
	"cmd/pkg/core/errors"

	"github.com/xfxdev/xlog"
)

func (a *ActionAdapter) PingDB() error {
	if err := a.storage.Conn().Ping(a.ctx); err != nil {
		xlog.Error(err)
		return errors.ErrDatabase
	}

	return nil
}
