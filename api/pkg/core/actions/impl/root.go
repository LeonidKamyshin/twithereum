package impl

import (
	"context"

	"cmd/database"
	"cmd/pkg/core/actions"
	"cmd/pkg/core/app/config"
)

type ActionAdapter struct {
	ctx context.Context
	cfg config.Config

	storage database.Storage
}

func NewActionAdapter(ctx context.Context, cfg config.Config, storage database.Storage) actions.Adapter {
	return &ActionAdapter{
		ctx:     ctx,
		cfg:     cfg,
		storage: storage,
	}
}
