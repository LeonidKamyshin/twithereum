package impl

import (
	"cmd/pkg/core/entities"
	"cmd/pkg/core/errors"

	"github.com/xfxdev/xlog"
)

const sqlGetFollowing = `
	WITH followers AS (
		SELECT f.id,
			   f.user_id,
			   f.followed_id,
			   u.username
		FROM tw.followers f
		LEFT JOIN tw.users u ON u.address = f.followed_id
		WHERE LOWER(f.user_id) = LOWER($1)
			AND ($3::bigint IS NULL OR f.id >= $3::bigint)
	), total_count AS (
		SELECT count(id) AS count FROM followers
	), following_limited AS (
		SELECT * FROM followers ORDER BY id LIMIT $2
	)
	SELECT
		id,
		followed_id,
		username,
		total_count.count
	FROM following_limited
	FULL OUTER JOIN total_count ON true;
`

func (a *ActionAdapter) GetFollowed(params entities.GetFollowersParams) ([]*entities.Follower, int64, error) {
	following := make([]*entities.Follower, 0)
	rows, err := a.storage.Conn().Query(a.ctx, sqlGetFollowing,
		params.Address,
		params.Limit,
		params.FirstID,
	)
	if err != nil {
		xlog.Error(err)
		return nil, 0, errors.ErrDatabase
	}

	var totalCount int64
	for rows.Next() {
		followed := entities.Follower{}

		err = rows.Scan(nil, nil, nil, &totalCount)
		if err != nil {
			xlog.Error(err)
			return nil, 0, errors.ErrDatabase
		}
		if totalCount == 0 {
			return following, totalCount, nil
		}

		err = rows.Scan(
			&followed.ID,
			&followed.Address,
			&followed.Username,
			&totalCount,
		)
		if err != nil {
			xlog.Error(err)
			return nil, 0, errors.ErrDatabase
		}

		following = append(following, &followed)
	}

	return following, totalCount, nil
}
