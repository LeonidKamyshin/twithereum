package impl

import (
	"testing"

	"cmd/pkg/core/entities"
	"cmd/pkg/core/errors"
	"github.com/stretchr/testify/suite"
)

func TestGetUserTestSuite(t *testing.T) {
	suite.Run(t, new(GetUserSuite))
}

type GetUserSuite struct {
	baseActionTestSuite

	user *entities.User
}

func (s *GetUserSuite) SetupSuite() {
	s.baseActionTestSuite.SetupSuite()
}

func (s *GetUserSuite) SetupTest() {
	s.baseActionTestSuite.SetupTest()
	s.user = &entities.User{
		Address:   "address",
		Username:  "username",
		Followers: 0,
		Followed:  0,
	}

	user := entities.User{
		Address:   "address12311",
		Username:  "username133441",
		Followers: 0,
		Followed:  0,
	}
	s.factory.CreateUser(user)
}

func (s *GetUserSuite) TestGetUser_OK() {
	s.factory.CreateUser(*s.user)
	params := entities.GetUserParams{Address: s.user.Address}

	res, err := s.actions.GetUser(params)

	s.Require().NoError(err)
	s.Require().Equal(s.user, res)
}

func (s *GetUserSuite) TestGetUser_NotFound() {
	params := entities.GetUserParams{Address: s.user.Address}

	_, err := s.actions.GetUser(params)
	expErr := errors.ErrUserNotFound.WithAddress(s.user.Address)

	s.checkCodedErr(expErr, err)
}
