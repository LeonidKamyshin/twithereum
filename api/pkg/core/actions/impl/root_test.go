package impl

import (
	"context"

	"cmd/database"
	"cmd/pkg/core/actions"
	"cmd/pkg/core/app/config"
	"cmd/pkg/core/errors"
	"cmd/pkg/tests"
	"cmd/pkg/utils"

	"github.com/stretchr/testify/suite"
)

type baseActionTestSuite struct {
	suite.Suite

	ctx context.Context
	cfg config.Config

	storage database.Storage

	actions actions.Adapter
	factory tests.Factory
}

func (s *baseActionTestSuite) SetupSuite() {
	s.cfg = config.Config{
		Postgres: config.Postgres{
			Host:        "127.0.0.1",
			Port:        5434,
			Password:    "123qwe",
			User:        "admin",
			Database:    "tw_test_db",
			MaxOpenConn: 20,
		},
	}
	s.storage = database.NewStorage(context.Background(), s.cfg)
}
func (s *baseActionTestSuite) SetupTest() {
	s.ctx = context.Background()
	s.factory = tests.NewFactory(s.ctx, s.storage)
	s.actions = NewActionAdapter(s.ctx, s.cfg, s.storage)
}

func (s *baseActionTestSuite) TearDownTest() {
	s.factory.CleanUp()
}

func (s *baseActionTestSuite) checkCodedErr(expected errors.CodedError, actual error) {
	s.Require().Error(actual)

	codedErr, ok := actual.(errors.CodedError)

	s.Require().True(ok)
	s.Require().True(utils.EqualMaps(expected.Details(), codedErr.Details()))
	s.Require().Equal(expected.CharCode(), codedErr.CharCode())
}
