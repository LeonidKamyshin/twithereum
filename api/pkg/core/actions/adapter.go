package actions

import (
	"cmd/pkg/core/entities"
)

//go:generate mockery --name Adapter --filename adapter.go
type Adapter interface {
	PingDB() error

	GetUser(params entities.GetUserParams) (*entities.User, error)
	GetUsersLike(params entities.GetUsersLikeParams) ([]*entities.UserInfo, int64, error)

	GetUserPosts(params entities.GetPostsParams) ([]*entities.Post, int64, error)
	GetFollowingPosts(params entities.GetPostsParams) ([]*entities.Post, int64, error)
	GetPosts(params entities.GetAllPostsParams) ([]*entities.Post, int64, error)

	GetFollowers(params entities.GetFollowersParams) ([]*entities.Follower, int64, error)
	GetFollowed(params entities.GetFollowersParams) ([]*entities.Follower, int64, error)
}
