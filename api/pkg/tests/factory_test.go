package tests

import (
	"context"
	"testing"
	"time"

	"cmd/database"
	"cmd/pkg/core/app/config"
	"cmd/pkg/core/entities"

	"github.com/stretchr/testify/suite"
)

func TestFactoryTestSuite(t *testing.T) {
	suite.Run(t, new(FactorySuite))
}

type FactorySuite struct {
	suite.Suite

	ctx     context.Context
	factory Factory
	storage database.Storage

	user1 entities.User
	user2 entities.User

	post entities.Post
}

func (s *FactorySuite) SetupSuite() {
	cfg := config.Config{
		Postgres: config.Postgres{
			Host:        "127.0.0.1",
			Port:        5434,
			Password:    "123qwe",
			User:        "admin",
			Database:    "tw_test_db",
			MaxOpenConn: 20,
		},
	}
	s.storage = database.NewStorage(context.Background(), cfg)
}

func (s *FactorySuite) SetupTest() {
	s.ctx = context.Background()
	s.factory = NewFactory(s.ctx, s.storage)

	s.user1 = entities.User{
		Address:   "address",
		Username:  "username",
		Followers: 0,
		Followed:  0,
	}
	s.user2 = entities.User{
		Address:   "address2",
		Username:  "username2",
		Followers: 0,
		Followed:  0,
	}
	s.post = entities.Post{
		Author:    s.user1.Address,
		ID:        1,
		Data:      make([]byte, 0),
		CreatedAt: time.Time{},
	}
}

func (s *FactorySuite) TearDownTest() {
	_, _ = s.factory.storage.Conn().Exec(
		s.ctx,
		`TRUNCATE TABLE tw.users, tw.posts, tw.followers RESTART IDENTITY CASCADE;`,
	)
}

func (s *FactorySuite) TestCreateUser() {
	s.factory.CreateUser(s.user1)

	row := s.factory.storage.Conn().QueryRow(
		s.ctx,
		`SELECT address, username, followers, followed FROM tw.users WHERE address=$1;`,
		s.user1.Address,
	)

	res := entities.User{}
	err := row.Scan(
		&res.Address,
		&res.Username,
		&res.Followers,
		&res.Followed,
	)

	s.Require().NoError(err)
	s.Require().Equal(s.user1, res)
}

func (s *FactorySuite) TestCreatePost() {
	s.factory.CreateUser(s.user1)
	s.factory.CreateUser(s.user2)
	s.factory.CreateFollower(s.user1.Address, s.user2.Address)

	row := s.factory.storage.Conn().QueryRow(
		s.ctx,
		`SELECT * FROM tw.followers WHERE user_id=$1 AND followed_id=$2;`,
		s.user1.Address,
		s.user2.Address,
	)

	var (
		id         int64
		userID     string
		followedID string
	)
	err := row.Scan(
		&id,
		&userID,
		&followedID,
	)

	s.Require().NoError(err)
	s.Require().Equal(userID, s.user1.Address)
	s.Require().Equal(followedID, s.user2.Address)
}

func (s *FactorySuite) TestCreateFollower() {
	user := entities.User{
		Address:   "address",
		Username:  "username",
		Followers: 10,
		Followed:  15,
	}

	s.factory.CreateUser(user)

	row := s.factory.storage.Conn().QueryRow(
		s.ctx,
		`SELECT address, username, followers, followed FROM tw.users WHERE address=$1;`,
		user.Address,
	)

	res := entities.User{}
	err := row.Scan(
		&res.Address,
		&res.Username,
		&res.Followers,
		&res.Followed,
	)

	s.Require().NoError(err)
	s.Require().Equal(user, res)
}
