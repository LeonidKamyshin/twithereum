package tests

import (
	"context"

	"cmd/database"
	"cmd/pkg/core/entities"
)

type Factory struct {
	ctx context.Context

	storage database.Storage
}

func NewFactory(ctx context.Context, storage database.Storage) Factory {
	factory := Factory{ctx: ctx, storage: storage}
	return factory
}

func (f *Factory) noErr(err error) {
	if err != nil {
		panic(err)
	}
}

func (f *Factory) CleanUp() {
	_, err := f.storage.Conn().Exec(
		f.ctx,
		`TRUNCATE TABLE tw.users, tw.posts, tw.followers RESTART IDENTITY CASCADE;`,
	)
	f.noErr(err)
}

func (f *Factory) CreateUsers(users ...entities.User) {
	for _, user := range users {
		f.CreateUser(user)
	}
}

func (f *Factory) CreateUser(user entities.User) {
	_, err := f.storage.Conn().Exec(
		f.ctx,
		`
			INSERT INTO tw.users (
			    address, username, followers, followed
			) VALUES (
			    $1, $2, $3, $4
			);
	    `,
		user.Address,
		user.Username,
		user.Followers,
		user.Followed,
	)

	f.noErr(err)
}

func (f *Factory) CreatePosts(posts ...entities.Post) {
	for _, post := range posts {
		f.CreatePost(post)
	}
}

func (f *Factory) CreatePost(post entities.Post) {
	_, err := f.storage.Conn().Exec(
		f.ctx,
		`
			INSERT INTO tw.posts (
			    id, address, p_data, created_at
			) VALUES (
			    $1, $2, $3, $4
			);
	    `,
		post.ID,
		post.Author,
		post.Data,
		post.CreatedAt,
	)

	f.noErr(err)
}

func (f *Factory) CreateFollowers(userAddress string, followedAddresses ...string) {
	for _, followedAddress := range followedAddresses {
		f.CreateFollower(userAddress, followedAddress)
	}
}

func (f *Factory) CreateFollower(userAddress string, followedAddress string) {
	_, err := f.storage.Conn().Exec(
		f.ctx,
		`
			INSERT INTO tw.followers (
			    user_id, followed_id
			) VALUES (
			    $1, $2
			);
	    `,
		userAddress,
		followedAddress,
	)

	f.noErr(err)
}
