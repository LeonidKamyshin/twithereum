package utils

import (
	"time"

	"github.com/google/go-cmp/cmp"
)

type Notify struct {
	ch chan interface{}
}

func NotifyOnce() *Notify {
	return &Notify{ch: make(chan interface{})}
}

func (n *Notify) Notify() {
	select {
	case n.ch <- struct{}{}:
	default:
	}
}

func (n *Notify) Wait() {
	<-n.ch
}

func (n *Notify) Done() <-chan interface{} {
	return n.ch
}

func ParseDateTime(value string) (time.Time, error) {
	return time.Parse(DateFormat, value)
}

func EqualMaps(first, second map[string]any) (bool, string) {
	eq := cmp.Equal(first, second)

	if eq {
		return eq, ""
	} else {
		return eq, cmp.Diff(first, second)
	}
}

func PointerTo[T any](v T) *T {
	return &v
}

func PointerArrayToSimple[T any](v []*T) []T {
	res := make([]T, len(v))
	for i, t := range v {
		res[i] = *t
	}
	return res
}
