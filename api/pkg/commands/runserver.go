package commands

import (
	"context"

	"cmd/pkg/server"
	"github.com/spf13/cobra"
	"github.com/xfxdev/xlog"
	"golang.org/x/xerrors"
)

func setupLogging(cmd *cobra.Command) error {
	logLevel, err := cmd.PersistentFlags().GetString("log-level")

	if err != nil {
		return err
	}

	if level, ok := xlog.ParseLevel(logLevel); !ok {
		return xerrors.New("Could not parse log level flag")
	} else {
		xlog.SetLevel(level)
	}
	return nil
}

func Runserver(cmd *cobra.Command, _ []string) {
	// setup logging.
	if err := setupLogging(cmd); err != nil {
		xlog.Error(err)
		return
	}

	// setup host.
	host, err := cmd.PersistentFlags().GetString("host")
	if err != nil {
		xlog.Error(err)
		return
	}

	// setup application
	application := server.NewRESTServer()
	if application == nil {
		return
	}
	application.Start(context.Background(), host)
}

func init() {
	MainCmd.PersistentFlags().String("log-level", "DEBUG", "log level")
	MainCmd.PersistentFlags().String("host", ":8001", "server host")
	MainCmd.PersistentFlags().String("config", "", "config path")
}

var MainCmd = &cobra.Command{
	Use:   "runserver",
	Short: "Run server with MM",
	Long:  "Run server with monitor manager",
	Run:   Runserver,
}
