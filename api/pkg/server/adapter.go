package server

import (
	"net/http"

	"cmd/pkg/core/actions"
	"cmd/pkg/core/actions/impl"
	"cmd/pkg/server/response"

	"github.com/xfxdev/xlog"
)

func (s *Server) repoContextHandlerAdapter(
	f func(http.ResponseWriter, *http.Request, actions.Adapter) error,
) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()

		actionsAdapter := impl.NewActionAdapter(ctx, s.cfg, s.storage)

		err := f(w, r, actionsAdapter)
		handleError(err, w)
	})
}

func handleError(err error, w http.ResponseWriter) {
	if err == nil {
		return
	}
	xlog.Errorf("Error handling request: %s", err.Error())
	err = response.APIError(w, err)
	if err != nil {
		xlog.Errorf("Error while preparing error response: %s", err.Error())
	}
}
