package server

import (
	"cmd/pkg/core/actions"
	"cmd/pkg/server/handlers"
	"cmd/pkg/server/response"
	"net/http"
)

type Route struct {
	Path    string
	Method  string
	Methods []string
	Fn      http.Handler
}

func (s *Server) GetRoutes() []Route {
	return []Route{
		{
			Path:   "/ping",
			Method: "GET",
			Fn:     s.repoContextHandlerAdapter(handlers.Ping),
		},
		{
			Path:   "/v0/user",
			Method: "GET",
			Fn:     s.repoContextHandlerAdapter(handlers.GetUser),
		},
		{
			Path:   "/v0/user/like",
			Method: "GET",
			Fn:     s.repoContextHandlerAdapter(handlers.GetUsersLike),
		},
		{
			Path:   "/v0/user/posts",
			Method: "GET",
			Fn:     s.repoContextHandlerAdapter(handlers.GetUserPosts),
		},
		{
			Path:   "/v0/posts/following",
			Method: "GET",
			Fn:     s.repoContextHandlerAdapter(handlers.GetFollowingPosts),
		},
		{
			Path:   "/v0/posts",
			Method: "GET",
			Fn:     s.repoContextHandlerAdapter(handlers.GetPosts),
		},
		{
			Path:   "/v0/user/followed",
			Method: "GET",
			Fn:     s.repoContextHandlerAdapter(handlers.GetFollowed),
		},
		{
			Path:   "/v0/user/followers",
			Method: "GET",
			Fn:     s.repoContextHandlerAdapter(handlers.GetFollowers),
		},
	}
}

func (s *Server) Route404(w http.ResponseWriter, _ *http.Request, _ actions.Adapter) error {
	return response.APIWithoutBody(w, http.StatusNotFound)
}
