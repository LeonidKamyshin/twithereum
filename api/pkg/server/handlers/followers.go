package handlers

import (
	"cmd/pkg/core/actions"
	"cmd/pkg/core/entities"
	"cmd/pkg/server/response"
	"net/http"
)

func GetFollowed(w http.ResponseWriter, r *http.Request, a actions.Adapter) error {
	request := entities.GetFollowersParams{}
	if err := entities.ParseAndValidateRequest(r, &request); err != nil {
		return err
	}

	followed, totalCount, err := a.GetFollowed(request)
	if err != nil {
		return err
	}

	resp := response.FollowingResponse{
		Followings: followed,
		TotalCount: totalCount,
	}
	return response.APICoded(w, http.StatusOK, resp)
}

func GetFollowers(w http.ResponseWriter, r *http.Request, a actions.Adapter) error {
	request := entities.GetFollowersParams{}
	if err := entities.ParseAndValidateRequest(r, &request); err != nil {
		return err
	}

	followed, totalCount, err := a.GetFollowers(request)
	if err != nil {
		return err
	}

	resp := response.FollowersResponse{
		Followers:  followed,
		TotalCount: totalCount,
	}
	return response.APICoded(w, http.StatusOK, resp)
}
