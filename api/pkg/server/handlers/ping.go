package handlers

import (
	"cmd/pkg/core/actions"
	"cmd/pkg/server/response"
	"net/http"
)

func Ping(w http.ResponseWriter, _ *http.Request, a actions.Adapter) error {
	err := a.PingDB()
	if err != nil {
		return err
	}
	return response.APIOK(w, response.PingResponse{Data: "OK"})
}
