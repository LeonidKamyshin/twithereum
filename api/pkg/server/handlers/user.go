package handlers

import (
	"cmd/pkg/core/actions"
	"cmd/pkg/core/entities"
	"cmd/pkg/server/response"
	"net/http"
)

func GetUser(w http.ResponseWriter, r *http.Request, a actions.Adapter) error {
	request := entities.GetUserParams{}
	if err := entities.ParseAndValidateRequest(r, &request); err != nil {
		return err
	}

	user, err := a.GetUser(request)
	if err != nil {
		return err
	}

	return response.APICoded(w, http.StatusOK, user)
}

func GetUsersLike(w http.ResponseWriter, r *http.Request, a actions.Adapter) error {
	request := entities.GetUsersLikeParams{}
	if err := entities.ParseAndValidateRequest(r, &request); err != nil {
		return err
	}

	users, totalCount, err := a.GetUsersLike(request)
	if err != nil {
		return err
	}

	resp := response.UsersResponse{
		Users:      users,
		TotalCount: totalCount,
	}
	return response.APICoded(w, http.StatusOK, resp)
}
