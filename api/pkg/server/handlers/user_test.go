package handlers

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"cmd/pkg/core/entities"

	"github.com/stretchr/testify/suite"
)

func TestGetUserTestSuite(t *testing.T) {
	suite.Run(t, new(GetUserTestSuite))
}

type GetUserTestSuite struct {
	baseHandlerSuite
}

func (s *GetUserTestSuite) TestGetUser_NoAddress() {
	req := httptest.NewRequest("GET", "http://localhost:8000/v0/user", nil)

	err := GetUser(s.response, req, nil)

	s.Require().Error(err)
}

func (s *GetUserTestSuite) TestGetUser_OK() {
	const address = "0x98eDD46f6360Ba35D4af1C5fdC9a137b0d87Db1c"
	req := httptest.NewRequest("GET", fmt.Sprintf("http://localhost:8000/v0/user?address=%s", address), nil)
	request := entities.GetUserParams{Address: address}
	user := entities.User{Address: address}
	s.actionAdapterMock.On("GetUser", request).Return(&user, nil)

	err := GetUser(s.response, req, nil)
	expResp := map[string]any{
		"address":   address,
		"username":  "",
		"followers": float64(0),
		"followed":  float64(0),
	}

	s.Require().NoError(err)
	s.checkResponse(expResp, http.StatusOK)
}
