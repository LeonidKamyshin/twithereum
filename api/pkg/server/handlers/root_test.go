package handlers

import (
	"cmd/mocks"
	"cmd/pkg/utils"
	"encoding/json"
	"net/http/httptest"

	"github.com/stretchr/testify/suite"
)

type baseHandlerSuite struct {
	suite.Suite

	response *httptest.ResponseRecorder

	actionAdapterMock *mocks.Adapter
}

func (s *baseHandlerSuite) SetupTest() {
	s.response = httptest.NewRecorder()
	s.actionAdapterMock = mocks.NewAdapter(s.T())
}

func (s *baseHandlerSuite) checkResponse(expBody map[string]any, expCode int) {
	s.Require().Equal(expCode, s.response.Code)

	actualResp := make(map[string]any)
	err := json.Unmarshal(s.response.Body.Bytes(), &actualResp)
	isEq, diff := utils.EqualMaps(expBody, actualResp)

	s.Require().NoError(err)
	s.Require().True(isEq, diff)
}
