package handlers

import (
	"cmd/pkg/core/actions"
	"cmd/pkg/core/entities"
	"cmd/pkg/server/response"
	"net/http"
)

func GetUserPosts(w http.ResponseWriter, r *http.Request, a actions.Adapter) error {
	request := entities.GetPostsParams{}
	if err := entities.ParseAndValidateRequest(r, &request); err != nil {
		return err
	}

	posts, totalCount, err := a.GetUserPosts(request)
	if err != nil {
		return err
	}

	resp := response.UserPostsResponse{
		Posts:      posts,
		TotalCount: totalCount,
	}
	return response.APICoded(w, http.StatusOK, resp)
}

func GetFollowingPosts(w http.ResponseWriter, r *http.Request, a actions.Adapter) error {
	request := entities.GetPostsParams{}
	if err := entities.ParseAndValidateRequest(r, &request); err != nil {
		return err
	}

	posts, totalCount, err := a.GetFollowingPosts(request)
	if err != nil {
		return err
	}

	resp := response.UserPostsResponse{
		Posts:      posts,
		TotalCount: totalCount,
	}
	return response.APICoded(w, http.StatusOK, resp)
}

func GetPosts(w http.ResponseWriter, r *http.Request, a actions.Adapter) error {
	request := entities.GetAllPostsParams{}
	if err := entities.ParseAndValidateRequest(r, &request); err != nil {
		return err
	}

	posts, totalCount, err := a.GetPosts(request)
	if err != nil {
		return err
	}

	resp := response.UserPostsResponse{
		Posts:      posts,
		TotalCount: totalCount,
	}
	return response.APICoded(w, http.StatusOK, resp)
}
