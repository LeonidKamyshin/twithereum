package response

import (
	"cmd/pkg/core/entities"
)

type FollowingResponse struct {
	Followings []*entities.Follower `json:"followings"`
	TotalCount int64                `json:"total_count"`
}
