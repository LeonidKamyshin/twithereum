package response

import (
	"cmd/pkg/core/entities"
)

type FollowersResponse struct {
	Followers  []*entities.Follower `json:"followers"`
	TotalCount int64                `json:"total_count"`
}
