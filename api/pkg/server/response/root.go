package response

import (
	"encoding/json"
	"errors"
	"net/http"

	coreerrors "cmd/pkg/core/errors"
)

func jsonResponse(statusCode int, w http.ResponseWriter, v any) error {
	if v == nil {
		v = map[string]any{}
	}
	respBytes, err := json.Marshal(v)

	if err != nil {
		return err
	}

	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(statusCode)
	_, err = w.Write(respBytes)

	return err
}

func APIOK(w http.ResponseWriter, data any) error {
	return jsonResponse(http.StatusOK, w, data)
}

func APIWithoutBody(w http.ResponseWriter, code int) error {
	w.WriteHeader(code)

	return nil
}

func APICoded(w http.ResponseWriter, code int, data any) error {
	return jsonResponse(code, w, data)
}

func APIError(w http.ResponseWriter, err error) error {
	statusCode := http.StatusInternalServerError
	var data any
	var codedErr coreerrors.CodedError

	if errors.As(err, &codedErr) {
		statusCode = codedErr.HTTPCode()
		if statusCode == http.StatusNotFound {
			return APIWithoutBody(w, statusCode)
		}
		params := map[string]any{
			"code":    codedErr.CharCode(),
			"message": codedErr.Description(),
		}
		if len(codedErr.Details()) > 0 {
			params["details"] = codedErr.Details()
		}
		data = params
	} else {
		data = map[string]string{
			"code":    "INTERNAL_ERROR",
			"message": err.Error(),
		}
	}

	return APICoded(w, statusCode, data)
}
