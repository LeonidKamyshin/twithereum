package response

import (
	"cmd/pkg/core/entities"
)

type UserPostsResponse struct {
	Posts      []*entities.Post `json:"posts"`
	TotalCount int64            `json:"total_count"`
}
