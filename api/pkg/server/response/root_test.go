package response

import (
	"cmd/pkg/core/errors"
	"encoding/json"
	"golang.org/x/xerrors"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestAPIOk(t *testing.T) {
	w := httptest.NewRecorder()

	err := APIOK(w, map[string]string{"some": "data"})

	assert.NoError(t, err)

	data := make(map[string]any)
	err = json.Unmarshal(w.Body.Bytes(), &data)

	assert.NoError(t, err)

	expData := map[string]any{
		"some": "data",
	}

	assert.Equal(t, http.StatusOK, w.Code)
	assert.Equal(t, "application/json; charset=utf-8", w.Header().Get("Content-Type"))
	assert.Equal(t, expData, data)
}

func TestAPIErrorGeneric(t *testing.T) {
	w := httptest.NewRecorder()

	err := APIError(w, xerrors.New("epic fail"))

	assert.NoError(t, err)

	data := make(map[string]any)
	err = json.Unmarshal(w.Body.Bytes(), &data)

	assert.NoError(t, err)

	expData := map[string]any{
		"code":    "INTERNAL_ERROR",
		"message": "epic fail",
	}

	assert.Equal(t, http.StatusInternalServerError, w.Code)
	assert.Equal(t, "application/json; charset=utf-8", w.Header().Get("Content-Type"))
	assert.Equal(t, expData, data)
}

func TestAPIError_CodedError(t *testing.T) {
	w := httptest.NewRecorder()
	codedErr := errors.ErrUserNotFound.WithAddress("777")

	err := APIError(w, codedErr)

	assert.NoError(t, err)

	data := make(map[string]any)
	err = json.Unmarshal(w.Body.Bytes(), &data)

	assert.NoError(t, err)

	expData := map[string]any{
		"code": "USER_NOT_FOUND",
		"details": map[string]any{
			"address": "777",
		},
		"message": "User not found.",
	}

	assert.Equal(t, http.StatusUnprocessableEntity, w.Code)
	assert.Equal(t, "application/json; charset=utf-8", w.Header().Get("Content-Type"))
	assert.Equal(t, expData, data)
}

func TestAPIError_CodedErrorWithoutDetails(t *testing.T) {
	w := httptest.NewRecorder()
	codedErr := errors.ErrUserNotFound

	err := APIError(w, codedErr)

	assert.NoError(t, err)

	data := make(map[string]any)
	err = json.Unmarshal(w.Body.Bytes(), &data)

	assert.NoError(t, err)

	expData := map[string]any{
		"code":    "USER_NOT_FOUND",
		"message": "User not found.",
	}

	assert.Equal(t, http.StatusUnprocessableEntity, w.Code)
	assert.Equal(t, "application/json; charset=utf-8", w.Header().Get("Content-Type"))
	assert.Equal(t, expData, data)
}

func TestAPIError_StatusNotFound(t *testing.T) {
	w := httptest.NewRecorder()
	codedErr := errors.ErrNotFound

	err := APIError(w, codedErr)

	assert.NoError(t, err)
	assert.Equal(t, http.StatusNotFound, w.Code)
	assert.Equal(t, "", w.Header().Get("Content-Type"))
}

func TestAPICoded_EmptyData(t *testing.T) {
	w := httptest.NewRecorder()

	err := APICoded(w, http.StatusCreated, nil)

	assert.NoError(t, err)
	assert.Equal(t, http.StatusCreated, w.Code)
	assert.Equal(t, "application/json; charset=utf-8", w.Header().Get("Content-Type"))
	assert.Equal(t, "{}", w.Body.String())
}
