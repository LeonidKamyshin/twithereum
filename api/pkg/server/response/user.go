package response

import (
	"cmd/pkg/core/entities"
)

type UsersResponse struct {
	Users      []*entities.UserInfo `json:"users"`
	TotalCount int64                `json:"total_count"`
}
