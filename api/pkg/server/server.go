package server

import (
	"context"
	"net/http"

	"cmd/database"
	"cmd/pkg/core/app/config"
	"cmd/pkg/server/middleware"
	"cmd/router"

	"github.com/xfxdev/xlog"
)

type Server struct {
	cfg     config.Config
	rootCtx context.Context

	router *router.ChiRouter

	storage database.Storage
}

func NewRESTServer() *Server {
	return &Server{
		cfg:     config.GetConfig(),
		rootCtx: context.Background(),
	}
}

func (s *Server) Start(_ context.Context, host string) {
	s.storage = database.NewStorage(s.rootCtx, s.cfg)
	s.router = router.NewChiRouter()

	s.InitMiddlewares()
	s.InitRoutes(s.GetRoutes())

	xlog.Infof("Initiating server on host [%s]", host)

	server := &http.Server{Addr: host, Handler: s.router}

	errorChan := make(chan error)

	go func() {
		if err := server.ListenAndServe(); err != nil {
			errorChan <- err
		}
	}()

	select {
	case err := <-errorChan:
		// server error
		xlog.Errorf("server error occurred: %v", err)
	}
}

func (s *Server) SetNotFoundHandler(fn http.Handler) {
	s.router.NotFound(fn.ServeHTTP)
}

func (s *Server) AddRoute(path string, fn http.Handler, methods ...string) {
	for _, method := range methods {
		s.router.Method(method, path, fn)
	}
}

func (s *Server) InitRoutes(routes []Route) {
	for _, p := range routes {
		fn := p.Fn
		if p.Method != "" {
			p.Methods = append(p.Methods, p.Method)
		}
		s.AddRoute(p.Path, fn, p.Methods...)
	}

	s.SetNotFoundHandler(s.repoContextHandlerAdapter(s.Route404))
}

func (s *Server) InitMiddlewares() {
	s.router.Use(middleware.LoggingMiddleware)

	if s.cfg.SwaggerUI.Enabled {
		s.router.Use(middleware.AddAccessControlHeaders())
		s.router.Use(
			func(next http.Handler) http.Handler {
				return middleware.SwaggerUI(middleware.SwaggerUIOpts{
					BasePath: "/",
					Path:     "docs",
					SpecURL:  "/specs/openapi.yml",
					Title:    "External API",
				}, next)
			},
		)
		fileServer := http.FileServer(http.Dir(s.cfg.SwaggerUI.SpecsDir))
		s.AddRoute("/specs/*", http.StripPrefix("/specs/", fileServer), http.MethodGet, http.MethodHead)
	}
}
