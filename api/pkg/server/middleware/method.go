package middleware

import (
	"net/http"

	"github.com/xfxdev/xlog"
	"golang.org/x/exp/slices"
)

var WHITELIST = []string{"/ping"}

func LoggingMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(writer http.ResponseWriter, r *http.Request) {

		blacked := slices.Contains(WHITELIST, r.URL.String())

		if !blacked {
			xlog.Debugf("[req start] [%s] [%s]", r.Method, r.URL.String())
		}

		next.ServeHTTP(writer, r)

		if !blacked {
			xlog.Debugf("[req finish]")
		}
	})
}
