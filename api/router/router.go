package router

import (
	"net/http"

	"github.com/go-chi/chi/v5"
)

const (
	NotFoundHandlerName = "notfound"
)

type ChiRouter struct {
	chi.Mux
}

func NewChiRouter() *ChiRouter {
	return &ChiRouter{*chi.NewRouter()}
}

func (c *ChiRouter) Use(mw func(http.Handler) http.Handler) {
	c.Mux.Use(mw)
}

func (c *ChiRouter) NotFound(handlerFn http.HandlerFunc) {
	c.Mux.NotFound(handlerFn)
}

func (c *ChiRouter) Method(method, pattern string, handler http.Handler) {
	c.Mux.Method(method, pattern, handler)
}

func (c *ChiRouter) Get(pattern string, handlerFn http.HandlerFunc) {
	c.Mux.Get(pattern, handlerFn)
}

func (c *ChiRouter) ProvideHandler() http.Handler {
	return &c.Mux
}

func (c *ChiRouter) GetRequestPathPattern(r *http.Request) string {
	routeContext := chi.RouteContext(r.Context())
	if routeContext == nil {
		return r.URL.Path
	}

	pattern := routeContext.RoutePattern()

	if pattern == "" {
		return NotFoundHandlerName

	}
	return pattern
}
