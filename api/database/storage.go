package database

import (
	"context"

	"cmd/pkg/core/app/config"
	"github.com/jackc/pgx/v5/pgxpool"
)

type Storage interface {
	Conn() *pgxpool.Pool
}

type storage struct {
	pool *pgxpool.Pool
}

func NewStorage(ctx context.Context, cfg config.Config) Storage {
	dbCfg, err := pgxpool.ParseConfig(cfg.Postgres.GetDSN())
	if err != nil {
		panic(err)
	}

	pool, err := pgxpool.NewWithConfig(ctx, dbCfg)
	if err != nil {
		panic(err)
	}
	return &storage{
		pool: pool,
	}
}

func (s *storage) Conn() *pgxpool.Pool {
	return s.pool
}
