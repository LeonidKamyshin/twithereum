package main

import (
	"cmd/internal/core/config"
	"cmd/internal/core/db/impl"
	"cmd/internal/core/listener"
	"context"
	"sync"

	"github.com/ethereum/go-ethereum/ethclient"
)

var wg sync.WaitGroup

func main() {
	wg.Add(1)

	ctx := context.Background()
	cfg := config.GetConfig()

	client, err := ethclient.Dial(cfg.Blockchain.ApiUrl)
	if err != nil {
		panic(err)
	}
	dbAdapter := impl.NewDBAdapter(ctx, cfg)
	eventListener := listener.NewListener(ctx, cfg, dbAdapter, client)
	go eventListener.Run()

	wg.Wait()
}
