package impl

const sqlUpdateLastBlock = `
	UPDATE tw.blockchain
	SET lastblock=$1;
`

func (a *DBAdapter) UpdateLastBlock(value int64) error {
	conn, err := a.dbConn()

	if err != nil {
		return err
	}

	_, err = conn.Exec(a.ctx, sqlUpdateLastBlock, value)
	if err != nil {
		return err
	}

	return nil
}
