package impl

import (
	"cmd/internal/core/entities"
)

const sqlDeleteFollower = `
	DELETE FROM tw.followers
       WHERE user_id=$1
	   AND followed_id=$2;
`

func (a *DBAdapter) DeleteFollower(follower entities.Follower) error {
	conn, err := a.dbConn()
	if err != nil {
		return err
	}

	_, err = conn.Exec(a.ctx, sqlDeleteFollower, follower.Follower, follower.Followed)
	if err != nil {
		return err
	}

	return nil
}
