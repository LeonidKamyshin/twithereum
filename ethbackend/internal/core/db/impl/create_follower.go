package impl

import (
	"cmd/internal/core/entities"
)

const sqlCreateFollower = `
	INSERT INTO tw.followers (user_id, followed_id)
	VALUES ($1, $2);
`

func (a *DBAdapter) CreateFollower(follower entities.Follower) error {
	conn, err := a.dbConn()
	if err != nil {
		return err
	}

	_, err = conn.Exec(a.ctx, sqlCreateFollower, follower.Follower, follower.Followed)
	if err != nil {
		return err
	}

	return nil
}
