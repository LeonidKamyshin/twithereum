package impl

import (
	"cmd/internal/core/entities"
)

const sqlCreatePost = `
	INSERT INTO tw.posts (id, address, p_data, created_at)
	VALUES ($1, $2, $3, $4);
`

func (a *DBAdapter) CreatePost(post entities.Post) error {
	conn, err := a.dbConn()
	if err != nil {
		return err
	}

	_, err = conn.Exec(a.ctx, sqlCreatePost,
		post.ID,
		post.Author,
		post.Data,
		post.CreatedAt,
	)
	if err != nil {
		return err
	}

	return nil
}
