package impl

const sqlGetLastBlock = `
	SELECT lastBlock 
	FROM tw.blockchain;
`

func (a *DBAdapter) GetLastBlock() (int64, error) {
	conn, err := a.dbConn()

	if err != nil {
		return 0, err
	}

	row := conn.QueryRow(a.ctx, sqlGetLastBlock)
	var lastBlock int64
	err = row.Scan(&lastBlock)
	if err != nil {
		return 0, err
	}

	return lastBlock, nil
}
