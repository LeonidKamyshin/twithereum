package impl

import (
	"cmd/internal/core/entities"
)

const sqlCreateUser = `
	INSERT INTO tw.users(address, username)
	VALUES ($1, $2);
`

func (a *DBAdapter) CreateUser(user entities.User) error {
	conn, err := a.dbConn()
	if err != nil {
		return err
	}

	_, err = conn.Exec(a.ctx, sqlCreateUser, user.Address, user.Username)
	if err != nil {
		return err
	}

	return nil
}
