package impl

const sqlDeletePost = `
	DELETE FROM tw.posts
       WHERE id=$1;
`

func (a *DBAdapter) DeletePost(postID int64) error {
	conn, err := a.dbConn()

	if err != nil {
		return err
	}

	_, err = conn.Exec(a.ctx, sqlDeletePost, postID)
	if err != nil {
		return err
	}

	return nil
}
