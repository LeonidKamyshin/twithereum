package impl

import (
	"cmd/internal/core/entities"
)

const sqlUpdateUsername = `
	UPDATE tw.users
	SET username=$2
	WHERE address = $1;
`

func (a *DBAdapter) UpdateUsername(user entities.User) error {
	conn, err := a.dbConn()

	if err != nil {
		return err
	}

	_, err = conn.Exec(a.ctx, sqlUpdateUsername, user.Address, user.Username)
	if err != nil {
		return err
	}

	return nil
}
