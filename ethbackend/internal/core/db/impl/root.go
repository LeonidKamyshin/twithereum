package impl

import (
	"cmd/internal/core/config"
	"cmd/internal/core/db"
	"context"

	"github.com/jackc/pgx/v5"
)

type DBAdapter struct {
	ctx  context.Context
	cfg  config.Config
	conn *pgx.Conn
	err  error
}

func NewDBAdapter(ctx context.Context, cfg config.Config) db.Adapter {
	return &DBAdapter{
		ctx: ctx,
		cfg: cfg,
	}
}

func (a *DBAdapter) dbConn() (*pgx.Conn, error) {
	if a.conn == nil {
		a.conn, a.err = pgx.Connect(a.ctx, a.cfg.Postgres.GetUrl())
	}
	if a.err != nil {
		return nil, a.err
	}
	return a.conn, nil
}

func (a *DBAdapter) Close() {
	if a.conn != nil {
		_ = a.conn.Close(a.ctx)
	}
}

func (a *DBAdapter) BeginTx() (*pgx.Tx, error) {
	conn, err := a.dbConn()
	if err != nil {
		return nil, err
	}

	tx, err := conn.Begin(a.ctx)
	if err != nil {
		return nil, err
	}
	return &tx, nil
}
