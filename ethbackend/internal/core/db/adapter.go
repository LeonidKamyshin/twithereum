package db

import (
	"cmd/internal/core/entities"

	"github.com/jackc/pgx/v5"
)

type Adapter interface {
	CreateUser(user entities.User) error
	UpdateUsername(user entities.User) error

	CreatePost(post entities.Post) error
	DeletePost(postID int64) error

	CreateFollower(follower entities.Follower) error
	DeleteFollower(follower entities.Follower) error

	GetLastBlock() (int64, error)
	UpdateLastBlock(value int64) error

	BeginTx() (*pgx.Tx, error)
	Close()
}
