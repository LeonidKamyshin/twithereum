package entities

import (
	"cmd/internal/core/solidity"
)

type User struct {
	Address  string
	Username string
}

func NewUserFromLog(log *solidity.PostServiceNewUser) User {
	return User{
		Address:  log.User.String(),
		Username: log.Username,
	}
}

func UsernameChangeFromLog(log *solidity.PostServiceUsernameChange) User {
	return User{
		Address:  log.User.String(),
		Username: log.Username,
	}
}
