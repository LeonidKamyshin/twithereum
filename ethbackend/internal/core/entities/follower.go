package entities

import (
	"cmd/internal/core/solidity"
)

type Follower struct {
	Follower string
	Followed string
}

func NewFollowerFromLog(log *solidity.PostServiceNewFollow) Follower {
	return Follower{
		Follower: log.Follower.String(),
		Followed: log.Followed.String(),
	}
}

func UnfollowFromLog(log *solidity.PostServiceUnfollow) Follower {
	return Follower{
		Follower: log.Follower.String(),
		Followed: log.Unfollowed.String(),
	}
}
