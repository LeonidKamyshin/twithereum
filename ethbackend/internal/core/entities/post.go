package entities

import (
	"cmd/internal/core/solidity"
	"time"
)

type Post struct {
	ID        int64
	Author    string
	Data      []byte
	CreatedAt time.Time
}

func NewPostFromLog(log *solidity.PostServiceNewPost) Post {
	return Post{
		ID:        log.PostId.Int64(),
		Author:    log.Author.String(),
		Data:      log.Post,
		CreatedAt: time.Unix(log.Timestamp.Int64(), 0),
	}
}
