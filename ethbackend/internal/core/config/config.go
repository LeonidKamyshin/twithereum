package config

import (
	"context"
	"fmt"
	"os"

	"github.com/heetch/confita"
	"github.com/heetch/confita/backend/file"
)

const (
	defaultConfigPath = "cmd/config/common.yaml"
)

type Postgres struct {
	Host     string `config:"host,required"`
	Port     int    `config:"port,required"`
	Password string `config:"password,required"`
	User     string `config:"user,required"`
	Database string `config:"database,required"`
}

func (p *Postgres) GetUrl() string {
	const template = "postgres://%s:%s@%s:%d/%s"
	return fmt.Sprintf(template, p.User, p.Password, p.Host, p.Port, p.Database)
}

type Blockchain struct {
	ApiUrl    string `yaml:"apiUrl"`
	Address   string `yaml:"address"`
	BatchSize int64  `yaml:"batchSize"`
}

type Config struct {
	Postgres   Postgres   `config:"postgres"`
	Blockchain Blockchain `yaml:"blockchain"`
}

func GetConfig() Config {

	configPath := defaultConfigPath

	if os.Getenv("TW_CONFIG") != "" {
		configPath = os.Getenv("TW_CONFIG")
	}

	loader := confita.NewLoader(
		file.NewBackend(configPath),
	)

	var cfg Config

	err := loader.Load(context.Background(), &cfg)

	if err != nil {
		panic(err)
	}
	return cfg
}
