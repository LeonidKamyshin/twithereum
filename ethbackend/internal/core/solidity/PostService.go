// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package solidity

import (
	"errors"
	"math/big"
	"strings"

	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/event"
)

// Reference imports to suppress errors if they are not otherwise used.
var (
	_ = errors.New
	_ = big.NewInt
	_ = strings.NewReader
	_ = ethereum.NotFound
	_ = bind.Bind
	_ = common.Big1
	_ = types.BloomLookup
	_ = event.NewSubscription
	_ = abi.ConvertType
)

// PostServicePostInfo is an auto generated low-level Go binding around an user-defined struct.
type PostServicePostInfo struct {
	PostId    *big.Int
	Author    common.Address
	Post      []byte
	Timestamp *big.Int
}

// PostServiceMetaData contains all meta data concerning the PostService contract.
var PostServiceMetaData = &bind.MetaData{
	ABI: "[{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"uint48\",\"name\":\"postId\",\"type\":\"uint48\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"timestamp\",\"type\":\"uint256\"}],\"name\":\"DeletePost\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"uint8\",\"name\":\"version\",\"type\":\"uint8\"}],\"name\":\"Initialized\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"address\",\"name\":\"follower\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"address\",\"name\":\"followed\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"timestamp\",\"type\":\"uint256\"}],\"name\":\"NewFollow\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"uint48\",\"name\":\"postId\",\"type\":\"uint48\"},{\"indexed\":false,\"internalType\":\"address\",\"name\":\"author\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"bytes\",\"name\":\"post\",\"type\":\"bytes\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"timestamp\",\"type\":\"uint256\"}],\"name\":\"NewPost\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"address\",\"name\":\"user\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"string\",\"name\":\"username\",\"type\":\"string\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"timestamp\",\"type\":\"uint256\"}],\"name\":\"NewUser\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"address\",\"name\":\"follower\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"address\",\"name\":\"unfollowed\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"timestamp\",\"type\":\"uint256\"}],\"name\":\"Unfollow\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"address\",\"name\":\"user\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"string\",\"name\":\"username\",\"type\":\"string\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"timestamp\",\"type\":\"uint256\"}],\"name\":\"UsernameChange\",\"type\":\"event\"},{\"inputs\":[],\"name\":\"ALREADY_FOLLOWED_ERROR\",\"outputs\":[{\"internalType\":\"string\",\"name\":\"\",\"type\":\"string\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"EMPTY_POST_ERROR\",\"outputs\":[{\"internalType\":\"string\",\"name\":\"\",\"type\":\"string\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"POST_DOESNT_EXIST_ERROR\",\"outputs\":[{\"internalType\":\"string\",\"name\":\"\",\"type\":\"string\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"POST_NOT_OWNED_ERROR\",\"outputs\":[{\"internalType\":\"string\",\"name\":\"\",\"type\":\"string\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"USERNAME_EMPTY_ERROR\",\"outputs\":[{\"internalType\":\"string\",\"name\":\"\",\"type\":\"string\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"USERNAME_TAKEN_ERROR\",\"outputs\":[{\"internalType\":\"string\",\"name\":\"\",\"type\":\"string\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"USER_ALREADY_EXIST_ERROR\",\"outputs\":[{\"internalType\":\"string\",\"name\":\"\",\"type\":\"string\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"USER_DOESNT_EXIST_ERROR\",\"outputs\":[{\"internalType\":\"string\",\"name\":\"\",\"type\":\"string\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"USER_NOT_FOLLOWED_ERROR\",\"outputs\":[{\"internalType\":\"string\",\"name\":\"\",\"type\":\"string\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"string\",\"name\":\"username\",\"type\":\"string\"}],\"name\":\"changeUsername\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint48\",\"name\":\"postId\",\"type\":\"uint48\"}],\"name\":\"deletePost\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"following\",\"type\":\"address\"}],\"name\":\"follow\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"followed\",\"outputs\":[{\"internalType\":\"uint32\",\"name\":\"\",\"type\":\"uint32\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"user\",\"type\":\"address\"}],\"name\":\"getFollowed\",\"outputs\":[{\"internalType\":\"uint32[]\",\"name\":\"\",\"type\":\"uint32[]\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"getPosts\",\"outputs\":[{\"components\":[{\"internalType\":\"uint48\",\"name\":\"postId\",\"type\":\"uint48\"},{\"internalType\":\"address\",\"name\":\"author\",\"type\":\"address\"},{\"internalType\":\"bytes\",\"name\":\"post\",\"type\":\"bytes\"},{\"internalType\":\"uint256\",\"name\":\"timestamp\",\"type\":\"uint256\"}],\"internalType\":\"structPostService.PostInfo[]\",\"name\":\"\",\"type\":\"tuple[]\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"getUsers\",\"outputs\":[{\"internalType\":\"address[]\",\"name\":\"\",\"type\":\"address[]\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"initBlock\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"initialize\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"user\",\"type\":\"address\"}],\"name\":\"isUserExist\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"bytes\",\"name\":\"post\",\"type\":\"bytes\"}],\"name\":\"makePost\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"postCount\",\"outputs\":[{\"internalType\":\"uint48\",\"name\":\"\",\"type\":\"uint48\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint48\",\"name\":\"\",\"type\":\"uint48\"}],\"name\":\"postIdToIdx\",\"outputs\":[{\"internalType\":\"uint48\",\"name\":\"\",\"type\":\"uint48\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"posts\",\"outputs\":[{\"internalType\":\"uint48\",\"name\":\"postId\",\"type\":\"uint48\"},{\"internalType\":\"address\",\"name\":\"author\",\"type\":\"address\"},{\"internalType\":\"bytes\",\"name\":\"post\",\"type\":\"bytes\"},{\"internalType\":\"uint256\",\"name\":\"timestamp\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"string\",\"name\":\"username\",\"type\":\"string\"}],\"name\":\"register\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"name\":\"registeredUsers\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"string\",\"name\":\"\",\"type\":\"string\"}],\"name\":\"takenUsernames\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"totalPost\",\"outputs\":[{\"internalType\":\"uint48\",\"name\":\"\",\"type\":\"uint48\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"unfollowing\",\"type\":\"address\"}],\"name\":\"unfollow\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"name\":\"userId\",\"outputs\":[{\"internalType\":\"uint32\",\"name\":\"\",\"type\":\"uint32\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"name\":\"usernames\",\"outputs\":[{\"internalType\":\"string\",\"name\":\"\",\"type\":\"string\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"name\":\"users\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"}]",
}

// PostServiceABI is the input ABI used to generate the binding from.
// Deprecated: Use PostServiceMetaData.ABI instead.
var PostServiceABI = PostServiceMetaData.ABI

// PostService is an auto generated Go binding around an Ethereum contract.
type PostService struct {
	PostServiceCaller     // Read-only binding to the contract
	PostServiceTransactor // Write-only binding to the contract
	PostServiceFilterer   // Log filterer for contract events
}

// PostServiceCaller is an auto generated read-only Go binding around an Ethereum contract.
type PostServiceCaller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// PostServiceTransactor is an auto generated write-only Go binding around an Ethereum contract.
type PostServiceTransactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// PostServiceFilterer is an auto generated log filtering Go binding around an Ethereum contract events.
type PostServiceFilterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// PostServiceSession is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type PostServiceSession struct {
	Contract     *PostService      // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// PostServiceCallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type PostServiceCallerSession struct {
	Contract *PostServiceCaller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts      // Call options to use throughout this session
}

// PostServiceTransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type PostServiceTransactorSession struct {
	Contract     *PostServiceTransactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts      // Transaction auth options to use throughout this session
}

// PostServiceRaw is an auto generated low-level Go binding around an Ethereum contract.
type PostServiceRaw struct {
	Contract *PostService // Generic contract binding to access the raw methods on
}

// PostServiceCallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type PostServiceCallerRaw struct {
	Contract *PostServiceCaller // Generic read-only contract binding to access the raw methods on
}

// PostServiceTransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type PostServiceTransactorRaw struct {
	Contract *PostServiceTransactor // Generic write-only contract binding to access the raw methods on
}

// NewPostService creates a new instance of PostService, bound to a specific deployed contract.
func NewPostService(address common.Address, backend bind.ContractBackend) (*PostService, error) {
	contract, err := bindPostService(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &PostService{PostServiceCaller: PostServiceCaller{contract: contract}, PostServiceTransactor: PostServiceTransactor{contract: contract}, PostServiceFilterer: PostServiceFilterer{contract: contract}}, nil
}

// NewPostServiceCaller creates a new read-only instance of PostService, bound to a specific deployed contract.
func NewPostServiceCaller(address common.Address, caller bind.ContractCaller) (*PostServiceCaller, error) {
	contract, err := bindPostService(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &PostServiceCaller{contract: contract}, nil
}

// NewPostServiceTransactor creates a new write-only instance of PostService, bound to a specific deployed contract.
func NewPostServiceTransactor(address common.Address, transactor bind.ContractTransactor) (*PostServiceTransactor, error) {
	contract, err := bindPostService(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &PostServiceTransactor{contract: contract}, nil
}

// NewPostServiceFilterer creates a new log filterer instance of PostService, bound to a specific deployed contract.
func NewPostServiceFilterer(address common.Address, filterer bind.ContractFilterer) (*PostServiceFilterer, error) {
	contract, err := bindPostService(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &PostServiceFilterer{contract: contract}, nil
}

// bindPostService binds a generic wrapper to an already deployed contract.
func bindPostService(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := PostServiceMetaData.GetAbi()
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, *parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_PostService *PostServiceRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _PostService.Contract.PostServiceCaller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_PostService *PostServiceRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _PostService.Contract.PostServiceTransactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_PostService *PostServiceRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _PostService.Contract.PostServiceTransactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_PostService *PostServiceCallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _PostService.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_PostService *PostServiceTransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _PostService.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_PostService *PostServiceTransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _PostService.Contract.contract.Transact(opts, method, params...)
}

// ALREADYFOLLOWEDERROR is a free data retrieval call binding the contract method 0x9bf35d5b.
//
// Solidity: function ALREADY_FOLLOWED_ERROR() view returns(string)
func (_PostService *PostServiceCaller) ALREADYFOLLOWEDERROR(opts *bind.CallOpts) (string, error) {
	var out []interface{}
	err := _PostService.contract.Call(opts, &out, "ALREADY_FOLLOWED_ERROR")

	if err != nil {
		return *new(string), err
	}

	out0 := *abi.ConvertType(out[0], new(string)).(*string)

	return out0, err

}

// ALREADYFOLLOWEDERROR is a free data retrieval call binding the contract method 0x9bf35d5b.
//
// Solidity: function ALREADY_FOLLOWED_ERROR() view returns(string)
func (_PostService *PostServiceSession) ALREADYFOLLOWEDERROR() (string, error) {
	return _PostService.Contract.ALREADYFOLLOWEDERROR(&_PostService.CallOpts)
}

// ALREADYFOLLOWEDERROR is a free data retrieval call binding the contract method 0x9bf35d5b.
//
// Solidity: function ALREADY_FOLLOWED_ERROR() view returns(string)
func (_PostService *PostServiceCallerSession) ALREADYFOLLOWEDERROR() (string, error) {
	return _PostService.Contract.ALREADYFOLLOWEDERROR(&_PostService.CallOpts)
}

// EMPTYPOSTERROR is a free data retrieval call binding the contract method 0xadc3e7f9.
//
// Solidity: function EMPTY_POST_ERROR() view returns(string)
func (_PostService *PostServiceCaller) EMPTYPOSTERROR(opts *bind.CallOpts) (string, error) {
	var out []interface{}
	err := _PostService.contract.Call(opts, &out, "EMPTY_POST_ERROR")

	if err != nil {
		return *new(string), err
	}

	out0 := *abi.ConvertType(out[0], new(string)).(*string)

	return out0, err

}

// EMPTYPOSTERROR is a free data retrieval call binding the contract method 0xadc3e7f9.
//
// Solidity: function EMPTY_POST_ERROR() view returns(string)
func (_PostService *PostServiceSession) EMPTYPOSTERROR() (string, error) {
	return _PostService.Contract.EMPTYPOSTERROR(&_PostService.CallOpts)
}

// EMPTYPOSTERROR is a free data retrieval call binding the contract method 0xadc3e7f9.
//
// Solidity: function EMPTY_POST_ERROR() view returns(string)
func (_PostService *PostServiceCallerSession) EMPTYPOSTERROR() (string, error) {
	return _PostService.Contract.EMPTYPOSTERROR(&_PostService.CallOpts)
}

// POSTDOESNTEXISTERROR is a free data retrieval call binding the contract method 0x2a2556ae.
//
// Solidity: function POST_DOESNT_EXIST_ERROR() view returns(string)
func (_PostService *PostServiceCaller) POSTDOESNTEXISTERROR(opts *bind.CallOpts) (string, error) {
	var out []interface{}
	err := _PostService.contract.Call(opts, &out, "POST_DOESNT_EXIST_ERROR")

	if err != nil {
		return *new(string), err
	}

	out0 := *abi.ConvertType(out[0], new(string)).(*string)

	return out0, err

}

// POSTDOESNTEXISTERROR is a free data retrieval call binding the contract method 0x2a2556ae.
//
// Solidity: function POST_DOESNT_EXIST_ERROR() view returns(string)
func (_PostService *PostServiceSession) POSTDOESNTEXISTERROR() (string, error) {
	return _PostService.Contract.POSTDOESNTEXISTERROR(&_PostService.CallOpts)
}

// POSTDOESNTEXISTERROR is a free data retrieval call binding the contract method 0x2a2556ae.
//
// Solidity: function POST_DOESNT_EXIST_ERROR() view returns(string)
func (_PostService *PostServiceCallerSession) POSTDOESNTEXISTERROR() (string, error) {
	return _PostService.Contract.POSTDOESNTEXISTERROR(&_PostService.CallOpts)
}

// POSTNOTOWNEDERROR is a free data retrieval call binding the contract method 0xc9ac23d0.
//
// Solidity: function POST_NOT_OWNED_ERROR() view returns(string)
func (_PostService *PostServiceCaller) POSTNOTOWNEDERROR(opts *bind.CallOpts) (string, error) {
	var out []interface{}
	err := _PostService.contract.Call(opts, &out, "POST_NOT_OWNED_ERROR")

	if err != nil {
		return *new(string), err
	}

	out0 := *abi.ConvertType(out[0], new(string)).(*string)

	return out0, err

}

// POSTNOTOWNEDERROR is a free data retrieval call binding the contract method 0xc9ac23d0.
//
// Solidity: function POST_NOT_OWNED_ERROR() view returns(string)
func (_PostService *PostServiceSession) POSTNOTOWNEDERROR() (string, error) {
	return _PostService.Contract.POSTNOTOWNEDERROR(&_PostService.CallOpts)
}

// POSTNOTOWNEDERROR is a free data retrieval call binding the contract method 0xc9ac23d0.
//
// Solidity: function POST_NOT_OWNED_ERROR() view returns(string)
func (_PostService *PostServiceCallerSession) POSTNOTOWNEDERROR() (string, error) {
	return _PostService.Contract.POSTNOTOWNEDERROR(&_PostService.CallOpts)
}

// USERNAMEEMPTYERROR is a free data retrieval call binding the contract method 0x10ab3933.
//
// Solidity: function USERNAME_EMPTY_ERROR() view returns(string)
func (_PostService *PostServiceCaller) USERNAMEEMPTYERROR(opts *bind.CallOpts) (string, error) {
	var out []interface{}
	err := _PostService.contract.Call(opts, &out, "USERNAME_EMPTY_ERROR")

	if err != nil {
		return *new(string), err
	}

	out0 := *abi.ConvertType(out[0], new(string)).(*string)

	return out0, err

}

// USERNAMEEMPTYERROR is a free data retrieval call binding the contract method 0x10ab3933.
//
// Solidity: function USERNAME_EMPTY_ERROR() view returns(string)
func (_PostService *PostServiceSession) USERNAMEEMPTYERROR() (string, error) {
	return _PostService.Contract.USERNAMEEMPTYERROR(&_PostService.CallOpts)
}

// USERNAMEEMPTYERROR is a free data retrieval call binding the contract method 0x10ab3933.
//
// Solidity: function USERNAME_EMPTY_ERROR() view returns(string)
func (_PostService *PostServiceCallerSession) USERNAMEEMPTYERROR() (string, error) {
	return _PostService.Contract.USERNAMEEMPTYERROR(&_PostService.CallOpts)
}

// USERNAMETAKENERROR is a free data retrieval call binding the contract method 0x46b5ae17.
//
// Solidity: function USERNAME_TAKEN_ERROR() view returns(string)
func (_PostService *PostServiceCaller) USERNAMETAKENERROR(opts *bind.CallOpts) (string, error) {
	var out []interface{}
	err := _PostService.contract.Call(opts, &out, "USERNAME_TAKEN_ERROR")

	if err != nil {
		return *new(string), err
	}

	out0 := *abi.ConvertType(out[0], new(string)).(*string)

	return out0, err

}

// USERNAMETAKENERROR is a free data retrieval call binding the contract method 0x46b5ae17.
//
// Solidity: function USERNAME_TAKEN_ERROR() view returns(string)
func (_PostService *PostServiceSession) USERNAMETAKENERROR() (string, error) {
	return _PostService.Contract.USERNAMETAKENERROR(&_PostService.CallOpts)
}

// USERNAMETAKENERROR is a free data retrieval call binding the contract method 0x46b5ae17.
//
// Solidity: function USERNAME_TAKEN_ERROR() view returns(string)
func (_PostService *PostServiceCallerSession) USERNAMETAKENERROR() (string, error) {
	return _PostService.Contract.USERNAMETAKENERROR(&_PostService.CallOpts)
}

// USERALREADYEXISTERROR is a free data retrieval call binding the contract method 0x6be8811e.
//
// Solidity: function USER_ALREADY_EXIST_ERROR() view returns(string)
func (_PostService *PostServiceCaller) USERALREADYEXISTERROR(opts *bind.CallOpts) (string, error) {
	var out []interface{}
	err := _PostService.contract.Call(opts, &out, "USER_ALREADY_EXIST_ERROR")

	if err != nil {
		return *new(string), err
	}

	out0 := *abi.ConvertType(out[0], new(string)).(*string)

	return out0, err

}

// USERALREADYEXISTERROR is a free data retrieval call binding the contract method 0x6be8811e.
//
// Solidity: function USER_ALREADY_EXIST_ERROR() view returns(string)
func (_PostService *PostServiceSession) USERALREADYEXISTERROR() (string, error) {
	return _PostService.Contract.USERALREADYEXISTERROR(&_PostService.CallOpts)
}

// USERALREADYEXISTERROR is a free data retrieval call binding the contract method 0x6be8811e.
//
// Solidity: function USER_ALREADY_EXIST_ERROR() view returns(string)
func (_PostService *PostServiceCallerSession) USERALREADYEXISTERROR() (string, error) {
	return _PostService.Contract.USERALREADYEXISTERROR(&_PostService.CallOpts)
}

// USERDOESNTEXISTERROR is a free data retrieval call binding the contract method 0x6abd8237.
//
// Solidity: function USER_DOESNT_EXIST_ERROR() view returns(string)
func (_PostService *PostServiceCaller) USERDOESNTEXISTERROR(opts *bind.CallOpts) (string, error) {
	var out []interface{}
	err := _PostService.contract.Call(opts, &out, "USER_DOESNT_EXIST_ERROR")

	if err != nil {
		return *new(string), err
	}

	out0 := *abi.ConvertType(out[0], new(string)).(*string)

	return out0, err

}

// USERDOESNTEXISTERROR is a free data retrieval call binding the contract method 0x6abd8237.
//
// Solidity: function USER_DOESNT_EXIST_ERROR() view returns(string)
func (_PostService *PostServiceSession) USERDOESNTEXISTERROR() (string, error) {
	return _PostService.Contract.USERDOESNTEXISTERROR(&_PostService.CallOpts)
}

// USERDOESNTEXISTERROR is a free data retrieval call binding the contract method 0x6abd8237.
//
// Solidity: function USER_DOESNT_EXIST_ERROR() view returns(string)
func (_PostService *PostServiceCallerSession) USERDOESNTEXISTERROR() (string, error) {
	return _PostService.Contract.USERDOESNTEXISTERROR(&_PostService.CallOpts)
}

// USERNOTFOLLOWEDERROR is a free data retrieval call binding the contract method 0x994d5d93.
//
// Solidity: function USER_NOT_FOLLOWED_ERROR() view returns(string)
func (_PostService *PostServiceCaller) USERNOTFOLLOWEDERROR(opts *bind.CallOpts) (string, error) {
	var out []interface{}
	err := _PostService.contract.Call(opts, &out, "USER_NOT_FOLLOWED_ERROR")

	if err != nil {
		return *new(string), err
	}

	out0 := *abi.ConvertType(out[0], new(string)).(*string)

	return out0, err

}

// USERNOTFOLLOWEDERROR is a free data retrieval call binding the contract method 0x994d5d93.
//
// Solidity: function USER_NOT_FOLLOWED_ERROR() view returns(string)
func (_PostService *PostServiceSession) USERNOTFOLLOWEDERROR() (string, error) {
	return _PostService.Contract.USERNOTFOLLOWEDERROR(&_PostService.CallOpts)
}

// USERNOTFOLLOWEDERROR is a free data retrieval call binding the contract method 0x994d5d93.
//
// Solidity: function USER_NOT_FOLLOWED_ERROR() view returns(string)
func (_PostService *PostServiceCallerSession) USERNOTFOLLOWEDERROR() (string, error) {
	return _PostService.Contract.USERNOTFOLLOWEDERROR(&_PostService.CallOpts)
}

// Followed is a free data retrieval call binding the contract method 0xbeea5212.
//
// Solidity: function followed(address , uint256 ) view returns(uint32)
func (_PostService *PostServiceCaller) Followed(opts *bind.CallOpts, arg0 common.Address, arg1 *big.Int) (uint32, error) {
	var out []interface{}
	err := _PostService.contract.Call(opts, &out, "followed", arg0, arg1)

	if err != nil {
		return *new(uint32), err
	}

	out0 := *abi.ConvertType(out[0], new(uint32)).(*uint32)

	return out0, err

}

// Followed is a free data retrieval call binding the contract method 0xbeea5212.
//
// Solidity: function followed(address , uint256 ) view returns(uint32)
func (_PostService *PostServiceSession) Followed(arg0 common.Address, arg1 *big.Int) (uint32, error) {
	return _PostService.Contract.Followed(&_PostService.CallOpts, arg0, arg1)
}

// Followed is a free data retrieval call binding the contract method 0xbeea5212.
//
// Solidity: function followed(address , uint256 ) view returns(uint32)
func (_PostService *PostServiceCallerSession) Followed(arg0 common.Address, arg1 *big.Int) (uint32, error) {
	return _PostService.Contract.Followed(&_PostService.CallOpts, arg0, arg1)
}

// GetFollowed is a free data retrieval call binding the contract method 0x0f977034.
//
// Solidity: function getFollowed(address user) view returns(uint32[])
func (_PostService *PostServiceCaller) GetFollowed(opts *bind.CallOpts, user common.Address) ([]uint32, error) {
	var out []interface{}
	err := _PostService.contract.Call(opts, &out, "getFollowed", user)

	if err != nil {
		return *new([]uint32), err
	}

	out0 := *abi.ConvertType(out[0], new([]uint32)).(*[]uint32)

	return out0, err

}

// GetFollowed is a free data retrieval call binding the contract method 0x0f977034.
//
// Solidity: function getFollowed(address user) view returns(uint32[])
func (_PostService *PostServiceSession) GetFollowed(user common.Address) ([]uint32, error) {
	return _PostService.Contract.GetFollowed(&_PostService.CallOpts, user)
}

// GetFollowed is a free data retrieval call binding the contract method 0x0f977034.
//
// Solidity: function getFollowed(address user) view returns(uint32[])
func (_PostService *PostServiceCallerSession) GetFollowed(user common.Address) ([]uint32, error) {
	return _PostService.Contract.GetFollowed(&_PostService.CallOpts, user)
}

// GetPosts is a free data retrieval call binding the contract method 0x41f3004a.
//
// Solidity: function getPosts() view returns((uint48,address,bytes,uint256)[])
func (_PostService *PostServiceCaller) GetPosts(opts *bind.CallOpts) ([]PostServicePostInfo, error) {
	var out []interface{}
	err := _PostService.contract.Call(opts, &out, "getPosts")

	if err != nil {
		return *new([]PostServicePostInfo), err
	}

	out0 := *abi.ConvertType(out[0], new([]PostServicePostInfo)).(*[]PostServicePostInfo)

	return out0, err

}

// GetPosts is a free data retrieval call binding the contract method 0x41f3004a.
//
// Solidity: function getPosts() view returns((uint48,address,bytes,uint256)[])
func (_PostService *PostServiceSession) GetPosts() ([]PostServicePostInfo, error) {
	return _PostService.Contract.GetPosts(&_PostService.CallOpts)
}

// GetPosts is a free data retrieval call binding the contract method 0x41f3004a.
//
// Solidity: function getPosts() view returns((uint48,address,bytes,uint256)[])
func (_PostService *PostServiceCallerSession) GetPosts() ([]PostServicePostInfo, error) {
	return _PostService.Contract.GetPosts(&_PostService.CallOpts)
}

// GetUsers is a free data retrieval call binding the contract method 0x00ce8e3e.
//
// Solidity: function getUsers() view returns(address[])
func (_PostService *PostServiceCaller) GetUsers(opts *bind.CallOpts) ([]common.Address, error) {
	var out []interface{}
	err := _PostService.contract.Call(opts, &out, "getUsers")

	if err != nil {
		return *new([]common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new([]common.Address)).(*[]common.Address)

	return out0, err

}

// GetUsers is a free data retrieval call binding the contract method 0x00ce8e3e.
//
// Solidity: function getUsers() view returns(address[])
func (_PostService *PostServiceSession) GetUsers() ([]common.Address, error) {
	return _PostService.Contract.GetUsers(&_PostService.CallOpts)
}

// GetUsers is a free data retrieval call binding the contract method 0x00ce8e3e.
//
// Solidity: function getUsers() view returns(address[])
func (_PostService *PostServiceCallerSession) GetUsers() ([]common.Address, error) {
	return _PostService.Contract.GetUsers(&_PostService.CallOpts)
}

// InitBlock is a free data retrieval call binding the contract method 0x5d05125b.
//
// Solidity: function initBlock() view returns(uint256)
func (_PostService *PostServiceCaller) InitBlock(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _PostService.contract.Call(opts, &out, "initBlock")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// InitBlock is a free data retrieval call binding the contract method 0x5d05125b.
//
// Solidity: function initBlock() view returns(uint256)
func (_PostService *PostServiceSession) InitBlock() (*big.Int, error) {
	return _PostService.Contract.InitBlock(&_PostService.CallOpts)
}

// InitBlock is a free data retrieval call binding the contract method 0x5d05125b.
//
// Solidity: function initBlock() view returns(uint256)
func (_PostService *PostServiceCallerSession) InitBlock() (*big.Int, error) {
	return _PostService.Contract.InitBlock(&_PostService.CallOpts)
}

// IsUserExist is a free data retrieval call binding the contract method 0x0255fa1e.
//
// Solidity: function isUserExist(address user) view returns(bool)
func (_PostService *PostServiceCaller) IsUserExist(opts *bind.CallOpts, user common.Address) (bool, error) {
	var out []interface{}
	err := _PostService.contract.Call(opts, &out, "isUserExist", user)

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// IsUserExist is a free data retrieval call binding the contract method 0x0255fa1e.
//
// Solidity: function isUserExist(address user) view returns(bool)
func (_PostService *PostServiceSession) IsUserExist(user common.Address) (bool, error) {
	return _PostService.Contract.IsUserExist(&_PostService.CallOpts, user)
}

// IsUserExist is a free data retrieval call binding the contract method 0x0255fa1e.
//
// Solidity: function isUserExist(address user) view returns(bool)
func (_PostService *PostServiceCallerSession) IsUserExist(user common.Address) (bool, error) {
	return _PostService.Contract.IsUserExist(&_PostService.CallOpts, user)
}

// PostCount is a free data retrieval call binding the contract method 0x17906c2e.
//
// Solidity: function postCount() view returns(uint48)
func (_PostService *PostServiceCaller) PostCount(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _PostService.contract.Call(opts, &out, "postCount")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// PostCount is a free data retrieval call binding the contract method 0x17906c2e.
//
// Solidity: function postCount() view returns(uint48)
func (_PostService *PostServiceSession) PostCount() (*big.Int, error) {
	return _PostService.Contract.PostCount(&_PostService.CallOpts)
}

// PostCount is a free data retrieval call binding the contract method 0x17906c2e.
//
// Solidity: function postCount() view returns(uint48)
func (_PostService *PostServiceCallerSession) PostCount() (*big.Int, error) {
	return _PostService.Contract.PostCount(&_PostService.CallOpts)
}

// PostIdToIdx is a free data retrieval call binding the contract method 0xa31a42de.
//
// Solidity: function postIdToIdx(uint48 ) view returns(uint48)
func (_PostService *PostServiceCaller) PostIdToIdx(opts *bind.CallOpts, arg0 *big.Int) (*big.Int, error) {
	var out []interface{}
	err := _PostService.contract.Call(opts, &out, "postIdToIdx", arg0)

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// PostIdToIdx is a free data retrieval call binding the contract method 0xa31a42de.
//
// Solidity: function postIdToIdx(uint48 ) view returns(uint48)
func (_PostService *PostServiceSession) PostIdToIdx(arg0 *big.Int) (*big.Int, error) {
	return _PostService.Contract.PostIdToIdx(&_PostService.CallOpts, arg0)
}

// PostIdToIdx is a free data retrieval call binding the contract method 0xa31a42de.
//
// Solidity: function postIdToIdx(uint48 ) view returns(uint48)
func (_PostService *PostServiceCallerSession) PostIdToIdx(arg0 *big.Int) (*big.Int, error) {
	return _PostService.Contract.PostIdToIdx(&_PostService.CallOpts, arg0)
}

// Posts is a free data retrieval call binding the contract method 0x0b1e7f83.
//
// Solidity: function posts(uint256 ) view returns(uint48 postId, address author, bytes post, uint256 timestamp)
func (_PostService *PostServiceCaller) Posts(opts *bind.CallOpts, arg0 *big.Int) (struct {
	PostId    *big.Int
	Author    common.Address
	Post      []byte
	Timestamp *big.Int
}, error) {
	var out []interface{}
	err := _PostService.contract.Call(opts, &out, "posts", arg0)

	outstruct := new(struct {
		PostId    *big.Int
		Author    common.Address
		Post      []byte
		Timestamp *big.Int
	})
	if err != nil {
		return *outstruct, err
	}

	outstruct.PostId = *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)
	outstruct.Author = *abi.ConvertType(out[1], new(common.Address)).(*common.Address)
	outstruct.Post = *abi.ConvertType(out[2], new([]byte)).(*[]byte)
	outstruct.Timestamp = *abi.ConvertType(out[3], new(*big.Int)).(**big.Int)

	return *outstruct, err

}

// Posts is a free data retrieval call binding the contract method 0x0b1e7f83.
//
// Solidity: function posts(uint256 ) view returns(uint48 postId, address author, bytes post, uint256 timestamp)
func (_PostService *PostServiceSession) Posts(arg0 *big.Int) (struct {
	PostId    *big.Int
	Author    common.Address
	Post      []byte
	Timestamp *big.Int
}, error) {
	return _PostService.Contract.Posts(&_PostService.CallOpts, arg0)
}

// Posts is a free data retrieval call binding the contract method 0x0b1e7f83.
//
// Solidity: function posts(uint256 ) view returns(uint48 postId, address author, bytes post, uint256 timestamp)
func (_PostService *PostServiceCallerSession) Posts(arg0 *big.Int) (struct {
	PostId    *big.Int
	Author    common.Address
	Post      []byte
	Timestamp *big.Int
}, error) {
	return _PostService.Contract.Posts(&_PostService.CallOpts, arg0)
}

// RegisteredUsers is a free data retrieval call binding the contract method 0x0e50cee5.
//
// Solidity: function registeredUsers(address ) view returns(bool)
func (_PostService *PostServiceCaller) RegisteredUsers(opts *bind.CallOpts, arg0 common.Address) (bool, error) {
	var out []interface{}
	err := _PostService.contract.Call(opts, &out, "registeredUsers", arg0)

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// RegisteredUsers is a free data retrieval call binding the contract method 0x0e50cee5.
//
// Solidity: function registeredUsers(address ) view returns(bool)
func (_PostService *PostServiceSession) RegisteredUsers(arg0 common.Address) (bool, error) {
	return _PostService.Contract.RegisteredUsers(&_PostService.CallOpts, arg0)
}

// RegisteredUsers is a free data retrieval call binding the contract method 0x0e50cee5.
//
// Solidity: function registeredUsers(address ) view returns(bool)
func (_PostService *PostServiceCallerSession) RegisteredUsers(arg0 common.Address) (bool, error) {
	return _PostService.Contract.RegisteredUsers(&_PostService.CallOpts, arg0)
}

// TakenUsernames is a free data retrieval call binding the contract method 0xe22554f6.
//
// Solidity: function takenUsernames(string ) view returns(bool)
func (_PostService *PostServiceCaller) TakenUsernames(opts *bind.CallOpts, arg0 string) (bool, error) {
	var out []interface{}
	err := _PostService.contract.Call(opts, &out, "takenUsernames", arg0)

	if err != nil {
		return *new(bool), err
	}

	out0 := *abi.ConvertType(out[0], new(bool)).(*bool)

	return out0, err

}

// TakenUsernames is a free data retrieval call binding the contract method 0xe22554f6.
//
// Solidity: function takenUsernames(string ) view returns(bool)
func (_PostService *PostServiceSession) TakenUsernames(arg0 string) (bool, error) {
	return _PostService.Contract.TakenUsernames(&_PostService.CallOpts, arg0)
}

// TakenUsernames is a free data retrieval call binding the contract method 0xe22554f6.
//
// Solidity: function takenUsernames(string ) view returns(bool)
func (_PostService *PostServiceCallerSession) TakenUsernames(arg0 string) (bool, error) {
	return _PostService.Contract.TakenUsernames(&_PostService.CallOpts, arg0)
}

// TotalPost is a free data retrieval call binding the contract method 0x56a1af20.
//
// Solidity: function totalPost() view returns(uint48)
func (_PostService *PostServiceCaller) TotalPost(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _PostService.contract.Call(opts, &out, "totalPost")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// TotalPost is a free data retrieval call binding the contract method 0x56a1af20.
//
// Solidity: function totalPost() view returns(uint48)
func (_PostService *PostServiceSession) TotalPost() (*big.Int, error) {
	return _PostService.Contract.TotalPost(&_PostService.CallOpts)
}

// TotalPost is a free data retrieval call binding the contract method 0x56a1af20.
//
// Solidity: function totalPost() view returns(uint48)
func (_PostService *PostServiceCallerSession) TotalPost() (*big.Int, error) {
	return _PostService.Contract.TotalPost(&_PostService.CallOpts)
}

// UserId is a free data retrieval call binding the contract method 0x376fe102.
//
// Solidity: function userId(address ) view returns(uint32)
func (_PostService *PostServiceCaller) UserId(opts *bind.CallOpts, arg0 common.Address) (uint32, error) {
	var out []interface{}
	err := _PostService.contract.Call(opts, &out, "userId", arg0)

	if err != nil {
		return *new(uint32), err
	}

	out0 := *abi.ConvertType(out[0], new(uint32)).(*uint32)

	return out0, err

}

// UserId is a free data retrieval call binding the contract method 0x376fe102.
//
// Solidity: function userId(address ) view returns(uint32)
func (_PostService *PostServiceSession) UserId(arg0 common.Address) (uint32, error) {
	return _PostService.Contract.UserId(&_PostService.CallOpts, arg0)
}

// UserId is a free data retrieval call binding the contract method 0x376fe102.
//
// Solidity: function userId(address ) view returns(uint32)
func (_PostService *PostServiceCallerSession) UserId(arg0 common.Address) (uint32, error) {
	return _PostService.Contract.UserId(&_PostService.CallOpts, arg0)
}

// Usernames is a free data retrieval call binding the contract method 0xee91877c.
//
// Solidity: function usernames(address ) view returns(string)
func (_PostService *PostServiceCaller) Usernames(opts *bind.CallOpts, arg0 common.Address) (string, error) {
	var out []interface{}
	err := _PostService.contract.Call(opts, &out, "usernames", arg0)

	if err != nil {
		return *new(string), err
	}

	out0 := *abi.ConvertType(out[0], new(string)).(*string)

	return out0, err

}

// Usernames is a free data retrieval call binding the contract method 0xee91877c.
//
// Solidity: function usernames(address ) view returns(string)
func (_PostService *PostServiceSession) Usernames(arg0 common.Address) (string, error) {
	return _PostService.Contract.Usernames(&_PostService.CallOpts, arg0)
}

// Usernames is a free data retrieval call binding the contract method 0xee91877c.
//
// Solidity: function usernames(address ) view returns(string)
func (_PostService *PostServiceCallerSession) Usernames(arg0 common.Address) (string, error) {
	return _PostService.Contract.Usernames(&_PostService.CallOpts, arg0)
}

// Users is a free data retrieval call binding the contract method 0x365b98b2.
//
// Solidity: function users(uint256 ) view returns(address)
func (_PostService *PostServiceCaller) Users(opts *bind.CallOpts, arg0 *big.Int) (common.Address, error) {
	var out []interface{}
	err := _PostService.contract.Call(opts, &out, "users", arg0)

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// Users is a free data retrieval call binding the contract method 0x365b98b2.
//
// Solidity: function users(uint256 ) view returns(address)
func (_PostService *PostServiceSession) Users(arg0 *big.Int) (common.Address, error) {
	return _PostService.Contract.Users(&_PostService.CallOpts, arg0)
}

// Users is a free data retrieval call binding the contract method 0x365b98b2.
//
// Solidity: function users(uint256 ) view returns(address)
func (_PostService *PostServiceCallerSession) Users(arg0 *big.Int) (common.Address, error) {
	return _PostService.Contract.Users(&_PostService.CallOpts, arg0)
}

// ChangeUsername is a paid mutator transaction binding the contract method 0x77c846af.
//
// Solidity: function changeUsername(string username) returns()
func (_PostService *PostServiceTransactor) ChangeUsername(opts *bind.TransactOpts, username string) (*types.Transaction, error) {
	return _PostService.contract.Transact(opts, "changeUsername", username)
}

// ChangeUsername is a paid mutator transaction binding the contract method 0x77c846af.
//
// Solidity: function changeUsername(string username) returns()
func (_PostService *PostServiceSession) ChangeUsername(username string) (*types.Transaction, error) {
	return _PostService.Contract.ChangeUsername(&_PostService.TransactOpts, username)
}

// ChangeUsername is a paid mutator transaction binding the contract method 0x77c846af.
//
// Solidity: function changeUsername(string username) returns()
func (_PostService *PostServiceTransactorSession) ChangeUsername(username string) (*types.Transaction, error) {
	return _PostService.Contract.ChangeUsername(&_PostService.TransactOpts, username)
}

// DeletePost is a paid mutator transaction binding the contract method 0x4768b0a0.
//
// Solidity: function deletePost(uint48 postId) returns()
func (_PostService *PostServiceTransactor) DeletePost(opts *bind.TransactOpts, postId *big.Int) (*types.Transaction, error) {
	return _PostService.contract.Transact(opts, "deletePost", postId)
}

// DeletePost is a paid mutator transaction binding the contract method 0x4768b0a0.
//
// Solidity: function deletePost(uint48 postId) returns()
func (_PostService *PostServiceSession) DeletePost(postId *big.Int) (*types.Transaction, error) {
	return _PostService.Contract.DeletePost(&_PostService.TransactOpts, postId)
}

// DeletePost is a paid mutator transaction binding the contract method 0x4768b0a0.
//
// Solidity: function deletePost(uint48 postId) returns()
func (_PostService *PostServiceTransactorSession) DeletePost(postId *big.Int) (*types.Transaction, error) {
	return _PostService.Contract.DeletePost(&_PostService.TransactOpts, postId)
}

// Follow is a paid mutator transaction binding the contract method 0x4dbf27cc.
//
// Solidity: function follow(address following) returns()
func (_PostService *PostServiceTransactor) Follow(opts *bind.TransactOpts, following common.Address) (*types.Transaction, error) {
	return _PostService.contract.Transact(opts, "follow", following)
}

// Follow is a paid mutator transaction binding the contract method 0x4dbf27cc.
//
// Solidity: function follow(address following) returns()
func (_PostService *PostServiceSession) Follow(following common.Address) (*types.Transaction, error) {
	return _PostService.Contract.Follow(&_PostService.TransactOpts, following)
}

// Follow is a paid mutator transaction binding the contract method 0x4dbf27cc.
//
// Solidity: function follow(address following) returns()
func (_PostService *PostServiceTransactorSession) Follow(following common.Address) (*types.Transaction, error) {
	return _PostService.Contract.Follow(&_PostService.TransactOpts, following)
}

// Initialize is a paid mutator transaction binding the contract method 0x8129fc1c.
//
// Solidity: function initialize() returns()
func (_PostService *PostServiceTransactor) Initialize(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _PostService.contract.Transact(opts, "initialize")
}

// Initialize is a paid mutator transaction binding the contract method 0x8129fc1c.
//
// Solidity: function initialize() returns()
func (_PostService *PostServiceSession) Initialize() (*types.Transaction, error) {
	return _PostService.Contract.Initialize(&_PostService.TransactOpts)
}

// Initialize is a paid mutator transaction binding the contract method 0x8129fc1c.
//
// Solidity: function initialize() returns()
func (_PostService *PostServiceTransactorSession) Initialize() (*types.Transaction, error) {
	return _PostService.Contract.Initialize(&_PostService.TransactOpts)
}

// MakePost is a paid mutator transaction binding the contract method 0x333a7200.
//
// Solidity: function makePost(bytes post) returns()
func (_PostService *PostServiceTransactor) MakePost(opts *bind.TransactOpts, post []byte) (*types.Transaction, error) {
	return _PostService.contract.Transact(opts, "makePost", post)
}

// MakePost is a paid mutator transaction binding the contract method 0x333a7200.
//
// Solidity: function makePost(bytes post) returns()
func (_PostService *PostServiceSession) MakePost(post []byte) (*types.Transaction, error) {
	return _PostService.Contract.MakePost(&_PostService.TransactOpts, post)
}

// MakePost is a paid mutator transaction binding the contract method 0x333a7200.
//
// Solidity: function makePost(bytes post) returns()
func (_PostService *PostServiceTransactorSession) MakePost(post []byte) (*types.Transaction, error) {
	return _PostService.Contract.MakePost(&_PostService.TransactOpts, post)
}

// Register is a paid mutator transaction binding the contract method 0xf2c298be.
//
// Solidity: function register(string username) returns()
func (_PostService *PostServiceTransactor) Register(opts *bind.TransactOpts, username string) (*types.Transaction, error) {
	return _PostService.contract.Transact(opts, "register", username)
}

// Register is a paid mutator transaction binding the contract method 0xf2c298be.
//
// Solidity: function register(string username) returns()
func (_PostService *PostServiceSession) Register(username string) (*types.Transaction, error) {
	return _PostService.Contract.Register(&_PostService.TransactOpts, username)
}

// Register is a paid mutator transaction binding the contract method 0xf2c298be.
//
// Solidity: function register(string username) returns()
func (_PostService *PostServiceTransactorSession) Register(username string) (*types.Transaction, error) {
	return _PostService.Contract.Register(&_PostService.TransactOpts, username)
}

// Unfollow is a paid mutator transaction binding the contract method 0x015a4ead.
//
// Solidity: function unfollow(address unfollowing) returns()
func (_PostService *PostServiceTransactor) Unfollow(opts *bind.TransactOpts, unfollowing common.Address) (*types.Transaction, error) {
	return _PostService.contract.Transact(opts, "unfollow", unfollowing)
}

// Unfollow is a paid mutator transaction binding the contract method 0x015a4ead.
//
// Solidity: function unfollow(address unfollowing) returns()
func (_PostService *PostServiceSession) Unfollow(unfollowing common.Address) (*types.Transaction, error) {
	return _PostService.Contract.Unfollow(&_PostService.TransactOpts, unfollowing)
}

// Unfollow is a paid mutator transaction binding the contract method 0x015a4ead.
//
// Solidity: function unfollow(address unfollowing) returns()
func (_PostService *PostServiceTransactorSession) Unfollow(unfollowing common.Address) (*types.Transaction, error) {
	return _PostService.Contract.Unfollow(&_PostService.TransactOpts, unfollowing)
}

// PostServiceDeletePostIterator is returned from FilterDeletePost and is used to iterate over the raw logs and unpacked data for DeletePost events raised by the PostService contract.
type PostServiceDeletePostIterator struct {
	Event *PostServiceDeletePost // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *PostServiceDeletePostIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(PostServiceDeletePost)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(PostServiceDeletePost)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *PostServiceDeletePostIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *PostServiceDeletePostIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// PostServiceDeletePost represents a DeletePost event raised by the PostService contract.
type PostServiceDeletePost struct {
	PostId    *big.Int
	Timestamp *big.Int
	Raw       types.Log // Blockchain specific contextual infos
}

// FilterDeletePost is a free log retrieval operation binding the contract event 0x52dc6f137974747004323b578ba60bd8004c2a532aababe7989cc1882afc8041.
//
// Solidity: event DeletePost(uint48 postId, uint256 timestamp)
func (_PostService *PostServiceFilterer) FilterDeletePost(opts *bind.FilterOpts) (*PostServiceDeletePostIterator, error) {

	logs, sub, err := _PostService.contract.FilterLogs(opts, "DeletePost")
	if err != nil {
		return nil, err
	}
	return &PostServiceDeletePostIterator{contract: _PostService.contract, event: "DeletePost", logs: logs, sub: sub}, nil
}

// WatchDeletePost is a free log subscription operation binding the contract event 0x52dc6f137974747004323b578ba60bd8004c2a532aababe7989cc1882afc8041.
//
// Solidity: event DeletePost(uint48 postId, uint256 timestamp)
func (_PostService *PostServiceFilterer) WatchDeletePost(opts *bind.WatchOpts, sink chan<- *PostServiceDeletePost) (event.Subscription, error) {

	logs, sub, err := _PostService.contract.WatchLogs(opts, "DeletePost")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(PostServiceDeletePost)
				if err := _PostService.contract.UnpackLog(event, "DeletePost", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseDeletePost is a log parse operation binding the contract event 0x52dc6f137974747004323b578ba60bd8004c2a532aababe7989cc1882afc8041.
//
// Solidity: event DeletePost(uint48 postId, uint256 timestamp)
func (_PostService *PostServiceFilterer) ParseDeletePost(log types.Log) (*PostServiceDeletePost, error) {
	event := new(PostServiceDeletePost)
	if err := _PostService.contract.UnpackLog(event, "DeletePost", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// PostServiceInitializedIterator is returned from FilterInitialized and is used to iterate over the raw logs and unpacked data for Initialized events raised by the PostService contract.
type PostServiceInitializedIterator struct {
	Event *PostServiceInitialized // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *PostServiceInitializedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(PostServiceInitialized)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(PostServiceInitialized)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *PostServiceInitializedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *PostServiceInitializedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// PostServiceInitialized represents a Initialized event raised by the PostService contract.
type PostServiceInitialized struct {
	Version uint8
	Raw     types.Log // Blockchain specific contextual infos
}

// FilterInitialized is a free log retrieval operation binding the contract event 0x7f26b83ff96e1f2b6a682f133852f6798a09c465da95921460cefb3847402498.
//
// Solidity: event Initialized(uint8 version)
func (_PostService *PostServiceFilterer) FilterInitialized(opts *bind.FilterOpts) (*PostServiceInitializedIterator, error) {

	logs, sub, err := _PostService.contract.FilterLogs(opts, "Initialized")
	if err != nil {
		return nil, err
	}
	return &PostServiceInitializedIterator{contract: _PostService.contract, event: "Initialized", logs: logs, sub: sub}, nil
}

// WatchInitialized is a free log subscription operation binding the contract event 0x7f26b83ff96e1f2b6a682f133852f6798a09c465da95921460cefb3847402498.
//
// Solidity: event Initialized(uint8 version)
func (_PostService *PostServiceFilterer) WatchInitialized(opts *bind.WatchOpts, sink chan<- *PostServiceInitialized) (event.Subscription, error) {

	logs, sub, err := _PostService.contract.WatchLogs(opts, "Initialized")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(PostServiceInitialized)
				if err := _PostService.contract.UnpackLog(event, "Initialized", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseInitialized is a log parse operation binding the contract event 0x7f26b83ff96e1f2b6a682f133852f6798a09c465da95921460cefb3847402498.
//
// Solidity: event Initialized(uint8 version)
func (_PostService *PostServiceFilterer) ParseInitialized(log types.Log) (*PostServiceInitialized, error) {
	event := new(PostServiceInitialized)
	if err := _PostService.contract.UnpackLog(event, "Initialized", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// PostServiceNewFollowIterator is returned from FilterNewFollow and is used to iterate over the raw logs and unpacked data for NewFollow events raised by the PostService contract.
type PostServiceNewFollowIterator struct {
	Event *PostServiceNewFollow // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *PostServiceNewFollowIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(PostServiceNewFollow)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(PostServiceNewFollow)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *PostServiceNewFollowIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *PostServiceNewFollowIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// PostServiceNewFollow represents a NewFollow event raised by the PostService contract.
type PostServiceNewFollow struct {
	Follower  common.Address
	Followed  common.Address
	Timestamp *big.Int
	Raw       types.Log // Blockchain specific contextual infos
}

// FilterNewFollow is a free log retrieval operation binding the contract event 0x46920c8305a92464a60e71e5c8f5959375d0efbe109bafb8ed0090b6dee8e9c8.
//
// Solidity: event NewFollow(address follower, address followed, uint256 timestamp)
func (_PostService *PostServiceFilterer) FilterNewFollow(opts *bind.FilterOpts) (*PostServiceNewFollowIterator, error) {

	logs, sub, err := _PostService.contract.FilterLogs(opts, "NewFollow")
	if err != nil {
		return nil, err
	}
	return &PostServiceNewFollowIterator{contract: _PostService.contract, event: "NewFollow", logs: logs, sub: sub}, nil
}

// WatchNewFollow is a free log subscription operation binding the contract event 0x46920c8305a92464a60e71e5c8f5959375d0efbe109bafb8ed0090b6dee8e9c8.
//
// Solidity: event NewFollow(address follower, address followed, uint256 timestamp)
func (_PostService *PostServiceFilterer) WatchNewFollow(opts *bind.WatchOpts, sink chan<- *PostServiceNewFollow) (event.Subscription, error) {

	logs, sub, err := _PostService.contract.WatchLogs(opts, "NewFollow")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(PostServiceNewFollow)
				if err := _PostService.contract.UnpackLog(event, "NewFollow", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseNewFollow is a log parse operation binding the contract event 0x46920c8305a92464a60e71e5c8f5959375d0efbe109bafb8ed0090b6dee8e9c8.
//
// Solidity: event NewFollow(address follower, address followed, uint256 timestamp)
func (_PostService *PostServiceFilterer) ParseNewFollow(log types.Log) (*PostServiceNewFollow, error) {
	event := new(PostServiceNewFollow)
	if err := _PostService.contract.UnpackLog(event, "NewFollow", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// PostServiceNewPostIterator is returned from FilterNewPost and is used to iterate over the raw logs and unpacked data for NewPost events raised by the PostService contract.
type PostServiceNewPostIterator struct {
	Event *PostServiceNewPost // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *PostServiceNewPostIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(PostServiceNewPost)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(PostServiceNewPost)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *PostServiceNewPostIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *PostServiceNewPostIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// PostServiceNewPost represents a NewPost event raised by the PostService contract.
type PostServiceNewPost struct {
	PostId    *big.Int
	Author    common.Address
	Post      []byte
	Timestamp *big.Int
	Raw       types.Log // Blockchain specific contextual infos
}

// FilterNewPost is a free log retrieval operation binding the contract event 0x342244549a10ac3d2c32a07d59d87618adc06d91a1461ec63477685f1376a148.
//
// Solidity: event NewPost(uint48 postId, address author, bytes post, uint256 timestamp)
func (_PostService *PostServiceFilterer) FilterNewPost(opts *bind.FilterOpts) (*PostServiceNewPostIterator, error) {

	logs, sub, err := _PostService.contract.FilterLogs(opts, "NewPost")
	if err != nil {
		return nil, err
	}
	return &PostServiceNewPostIterator{contract: _PostService.contract, event: "NewPost", logs: logs, sub: sub}, nil
}

// WatchNewPost is a free log subscription operation binding the contract event 0x342244549a10ac3d2c32a07d59d87618adc06d91a1461ec63477685f1376a148.
//
// Solidity: event NewPost(uint48 postId, address author, bytes post, uint256 timestamp)
func (_PostService *PostServiceFilterer) WatchNewPost(opts *bind.WatchOpts, sink chan<- *PostServiceNewPost) (event.Subscription, error) {

	logs, sub, err := _PostService.contract.WatchLogs(opts, "NewPost")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(PostServiceNewPost)
				if err := _PostService.contract.UnpackLog(event, "NewPost", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseNewPost is a log parse operation binding the contract event 0x342244549a10ac3d2c32a07d59d87618adc06d91a1461ec63477685f1376a148.
//
// Solidity: event NewPost(uint48 postId, address author, bytes post, uint256 timestamp)
func (_PostService *PostServiceFilterer) ParseNewPost(log types.Log) (*PostServiceNewPost, error) {
	event := new(PostServiceNewPost)
	if err := _PostService.contract.UnpackLog(event, "NewPost", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// PostServiceNewUserIterator is returned from FilterNewUser and is used to iterate over the raw logs and unpacked data for NewUser events raised by the PostService contract.
type PostServiceNewUserIterator struct {
	Event *PostServiceNewUser // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *PostServiceNewUserIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(PostServiceNewUser)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(PostServiceNewUser)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *PostServiceNewUserIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *PostServiceNewUserIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// PostServiceNewUser represents a NewUser event raised by the PostService contract.
type PostServiceNewUser struct {
	User      common.Address
	Username  string
	Timestamp *big.Int
	Raw       types.Log // Blockchain specific contextual infos
}

// FilterNewUser is a free log retrieval operation binding the contract event 0x329d359cf9f4051167ad17cee4f7c13e33128369772783e288c19fcdd1384fed.
//
// Solidity: event NewUser(address user, string username, uint256 timestamp)
func (_PostService *PostServiceFilterer) FilterNewUser(opts *bind.FilterOpts) (*PostServiceNewUserIterator, error) {

	logs, sub, err := _PostService.contract.FilterLogs(opts, "NewUser")
	if err != nil {
		return nil, err
	}
	return &PostServiceNewUserIterator{contract: _PostService.contract, event: "NewUser", logs: logs, sub: sub}, nil
}

// WatchNewUser is a free log subscription operation binding the contract event 0x329d359cf9f4051167ad17cee4f7c13e33128369772783e288c19fcdd1384fed.
//
// Solidity: event NewUser(address user, string username, uint256 timestamp)
func (_PostService *PostServiceFilterer) WatchNewUser(opts *bind.WatchOpts, sink chan<- *PostServiceNewUser) (event.Subscription, error) {

	logs, sub, err := _PostService.contract.WatchLogs(opts, "NewUser")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(PostServiceNewUser)
				if err := _PostService.contract.UnpackLog(event, "NewUser", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseNewUser is a log parse operation binding the contract event 0x329d359cf9f4051167ad17cee4f7c13e33128369772783e288c19fcdd1384fed.
//
// Solidity: event NewUser(address user, string username, uint256 timestamp)
func (_PostService *PostServiceFilterer) ParseNewUser(log types.Log) (*PostServiceNewUser, error) {
	event := new(PostServiceNewUser)
	if err := _PostService.contract.UnpackLog(event, "NewUser", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// PostServiceUnfollowIterator is returned from FilterUnfollow and is used to iterate over the raw logs and unpacked data for Unfollow events raised by the PostService contract.
type PostServiceUnfollowIterator struct {
	Event *PostServiceUnfollow // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *PostServiceUnfollowIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(PostServiceUnfollow)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(PostServiceUnfollow)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *PostServiceUnfollowIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *PostServiceUnfollowIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// PostServiceUnfollow represents a Unfollow event raised by the PostService contract.
type PostServiceUnfollow struct {
	Follower   common.Address
	Unfollowed common.Address
	Timestamp  *big.Int
	Raw        types.Log // Blockchain specific contextual infos
}

// FilterUnfollow is a free log retrieval operation binding the contract event 0x0c71468a2592077189a7e3464c3318d8c2d4db7f339f95cd326d3d03d7179895.
//
// Solidity: event Unfollow(address follower, address unfollowed, uint256 timestamp)
func (_PostService *PostServiceFilterer) FilterUnfollow(opts *bind.FilterOpts) (*PostServiceUnfollowIterator, error) {

	logs, sub, err := _PostService.contract.FilterLogs(opts, "Unfollow")
	if err != nil {
		return nil, err
	}
	return &PostServiceUnfollowIterator{contract: _PostService.contract, event: "Unfollow", logs: logs, sub: sub}, nil
}

// WatchUnfollow is a free log subscription operation binding the contract event 0x0c71468a2592077189a7e3464c3318d8c2d4db7f339f95cd326d3d03d7179895.
//
// Solidity: event Unfollow(address follower, address unfollowed, uint256 timestamp)
func (_PostService *PostServiceFilterer) WatchUnfollow(opts *bind.WatchOpts, sink chan<- *PostServiceUnfollow) (event.Subscription, error) {

	logs, sub, err := _PostService.contract.WatchLogs(opts, "Unfollow")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(PostServiceUnfollow)
				if err := _PostService.contract.UnpackLog(event, "Unfollow", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseUnfollow is a log parse operation binding the contract event 0x0c71468a2592077189a7e3464c3318d8c2d4db7f339f95cd326d3d03d7179895.
//
// Solidity: event Unfollow(address follower, address unfollowed, uint256 timestamp)
func (_PostService *PostServiceFilterer) ParseUnfollow(log types.Log) (*PostServiceUnfollow, error) {
	event := new(PostServiceUnfollow)
	if err := _PostService.contract.UnpackLog(event, "Unfollow", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// PostServiceUsernameChangeIterator is returned from FilterUsernameChange and is used to iterate over the raw logs and unpacked data for UsernameChange events raised by the PostService contract.
type PostServiceUsernameChangeIterator struct {
	Event *PostServiceUsernameChange // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *PostServiceUsernameChangeIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(PostServiceUsernameChange)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(PostServiceUsernameChange)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *PostServiceUsernameChangeIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *PostServiceUsernameChangeIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// PostServiceUsernameChange represents a UsernameChange event raised by the PostService contract.
type PostServiceUsernameChange struct {
	User      common.Address
	Username  string
	Timestamp *big.Int
	Raw       types.Log // Blockchain specific contextual infos
}

// FilterUsernameChange is a free log retrieval operation binding the contract event 0x42478efff2dc7fca7be11da1ea250aef6cd7b0e4b72205ee7df396a4813a4bd9.
//
// Solidity: event UsernameChange(address user, string username, uint256 timestamp)
func (_PostService *PostServiceFilterer) FilterUsernameChange(opts *bind.FilterOpts) (*PostServiceUsernameChangeIterator, error) {

	logs, sub, err := _PostService.contract.FilterLogs(opts, "UsernameChange")
	if err != nil {
		return nil, err
	}
	return &PostServiceUsernameChangeIterator{contract: _PostService.contract, event: "UsernameChange", logs: logs, sub: sub}, nil
}

// WatchUsernameChange is a free log subscription operation binding the contract event 0x42478efff2dc7fca7be11da1ea250aef6cd7b0e4b72205ee7df396a4813a4bd9.
//
// Solidity: event UsernameChange(address user, string username, uint256 timestamp)
func (_PostService *PostServiceFilterer) WatchUsernameChange(opts *bind.WatchOpts, sink chan<- *PostServiceUsernameChange) (event.Subscription, error) {

	logs, sub, err := _PostService.contract.WatchLogs(opts, "UsernameChange")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(PostServiceUsernameChange)
				if err := _PostService.contract.UnpackLog(event, "UsernameChange", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseUsernameChange is a log parse operation binding the contract event 0x42478efff2dc7fca7be11da1ea250aef6cd7b0e4b72205ee7df396a4813a4bd9.
//
// Solidity: event UsernameChange(address user, string username, uint256 timestamp)
func (_PostService *PostServiceFilterer) ParseUsernameChange(log types.Log) (*PostServiceUsernameChange, error) {
	event := new(PostServiceUsernameChange)
	if err := _PostService.contract.UnpackLog(event, "UsernameChange", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}
