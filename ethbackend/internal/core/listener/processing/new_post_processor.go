package processing

import (
	"cmd/internal/core/db"
	"cmd/internal/core/entities"
	"cmd/internal/core/solidity"

	"github.com/ethereum/go-ethereum/core/types"
)

func ProcessNewPostLog(log types.Log, adapter db.Adapter, postService *solidity.PostService) error {
	postLog, err := postService.ParseNewPost(log)
	if err == nil {
		dbError := adapter.CreatePost(entities.NewPostFromLog(postLog))
		if dbError != nil {
			return dbError
		}
	}
	return nil
}
