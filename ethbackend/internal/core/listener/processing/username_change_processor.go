package processing

import (
	"cmd/internal/core/db"
	"cmd/internal/core/entities"
	"cmd/internal/core/solidity"

	"github.com/ethereum/go-ethereum/core/types"
)

func ProcessUsernameChangeLog(log types.Log, adapter db.Adapter, postService *solidity.PostService) error {
	usernameChangeLog, err := postService.ParseUsernameChange(log)
	if err == nil {
		dbError := adapter.UpdateUsername(entities.UsernameChangeFromLog(usernameChangeLog))
		if dbError != nil {
			return dbError
		}
	}
	return nil
}
