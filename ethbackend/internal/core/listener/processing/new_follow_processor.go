package processing

import (
	"cmd/internal/core/db"
	"cmd/internal/core/entities"
	"cmd/internal/core/solidity"

	"github.com/ethereum/go-ethereum/core/types"
)

func ProcessNewFollowLog(log types.Log, adapter db.Adapter, postService *solidity.PostService) error {
	followLog, err := postService.ParseNewFollow(log)
	if err == nil {
		dbError := adapter.CreateFollower(entities.NewFollowerFromLog(followLog))
		if dbError != nil {
			return dbError
		}
	}
	return nil
}
