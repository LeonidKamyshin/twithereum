package processing

import (
	"cmd/internal/core/db"
	"cmd/internal/core/entities"
	"cmd/internal/core/solidity"

	"github.com/ethereum/go-ethereum/core/types"
)

func ProcessUnfollowLog(log types.Log, adapter db.Adapter, postService *solidity.PostService) error {
	unfollowLog, err := postService.ParseUnfollow(log)
	if err == nil {
		dbError := adapter.DeleteFollower(entities.UnfollowFromLog(unfollowLog))
		if dbError != nil {
			return dbError
		}
	}
	return nil
}
