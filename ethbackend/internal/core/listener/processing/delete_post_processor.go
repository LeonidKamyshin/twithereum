package processing

import (
	"cmd/internal/core/db"
	"cmd/internal/core/solidity"

	"github.com/ethereum/go-ethereum/core/types"
)

func ProcessDeletePostLog(log types.Log, adapter db.Adapter, postService *solidity.PostService) error {
	deletePostLog, err := postService.ParseDeletePost(log)
	if err == nil {
		dbError := adapter.DeletePost(deletePostLog.PostId.Int64())
		if dbError != nil {
			return dbError
		}
	}
	return nil
}
