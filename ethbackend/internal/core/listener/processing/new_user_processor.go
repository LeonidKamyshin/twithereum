package processing

import (
	"cmd/internal/core/db"
	"cmd/internal/core/entities"
	"cmd/internal/core/solidity"

	"github.com/ethereum/go-ethereum/core/types"
)

func ProcessNewUserLog(log types.Log, adapter db.Adapter, postService *solidity.PostService) error {
	userLog, err := postService.ParseNewUser(log)
	if err == nil {
		dbError := adapter.CreateUser(entities.NewUserFromLog(userLog))
		if dbError != nil {
			return dbError
		}
	}
	return nil
}
