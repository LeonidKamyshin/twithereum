package processing

import (
	"cmd/internal/core/db"
	"cmd/internal/core/solidity"

	"github.com/ethereum/go-ethereum/core/types"
)

var logProcessable = []func(log types.Log, adapter db.Adapter, postService *solidity.PostService) error{
	ProcessNewUserLog,
	ProcessNewPostLog,
	ProcessNewFollowLog,
	ProcessDeletePostLog,
	ProcessUnfollowLog,
	ProcessUsernameChangeLog,
}

type Processors struct {
	adapter     db.Adapter
	postService *solidity.PostService

	logProcessable []func(log types.Log, adapter db.Adapter, postService *solidity.PostService) error
}

func NewProcessors(adapter db.Adapter, postService *solidity.PostService) *Processors {
	return &Processors{
		adapter:        adapter,
		postService:    postService,
		logProcessable: logProcessable,
	}
}

func (p *Processors) Process(log types.Log) error {
	for _, processor := range p.logProcessable {
		err := processor(log, p.adapter, p.postService)
		if err != nil {
			return err
		}
	}
	return nil
}
