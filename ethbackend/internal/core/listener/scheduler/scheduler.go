package scheduler

import (
	"cmd/internal/core/db"
	"cmd/internal/core/solidity"
	"math/big"

	"github.com/ethereum/go-ethereum/common/math"
	"github.com/ethereum/go-ethereum/ethclient"
)

type BlockScheduler struct {
	postService *solidity.PostService
	client      *ethclient.Client
	adapter     db.Adapter
	batchSize   int64
}

func NewBlockScheduler(
	postService *solidity.PostService,
	client *ethclient.Client,
	adapter db.Adapter,
	batchSize int64,
) *BlockScheduler {

	return &BlockScheduler{
		postService: postService,
		client:      client,
		adapter:     adapter,
		batchSize:   batchSize,
	}
}

func (s *BlockScheduler) GetCurrentBlock() (*big.Int, error) {
	lastBlock, err := s.adapter.GetLastBlock()
	if err != nil {
		return nil, err
	}

	initBlock, err := s.postService.InitBlock(nil)
	if err != nil {
		return nil, err
	}
	currentBlock := math.BigMax(big.NewInt(lastBlock+1), initBlock)
	return currentBlock, nil
}

func (s *BlockScheduler) GetToBlock(currentBlock *big.Int, headerBlock *big.Int) *big.Int {
	toBlock := big.NewInt(0)
	toBlock.Add(currentBlock, big.NewInt(s.batchSize-1))
	if toBlock.Cmp(headerBlock) == 1 {
		return currentBlock
	}
	return toBlock
}
