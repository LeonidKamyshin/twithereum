package listener

import (
	"cmd/internal/core/config"
	"cmd/internal/core/db"
	"cmd/internal/core/listener/processing"
	"cmd/internal/core/listener/scheduler"
	"cmd/internal/core/solidity"
	"context"
	"log"
	"math/big"

	"github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/ethclient"
)

type Listener struct {
	ctx     context.Context
	cfg     config.Config
	adapter db.Adapter

	client          *ethclient.Client
	contractAddress common.Address
	postService     *solidity.PostService

	processors     *processing.Processors
	blockScheduler *scheduler.BlockScheduler
}

func NewListener(
	ctx context.Context,
	cfg config.Config,
	adapter db.Adapter,
	client *ethclient.Client,
) *Listener {

	contractAddress := common.HexToAddress(cfg.Blockchain.Address)
	postService, err := solidity.NewPostService(contractAddress, client)
	if err != nil {
		panic(err)
	}

	return &Listener{
		ctx:             ctx,
		adapter:         adapter,
		client:          client,
		contractAddress: contractAddress,
		postService:     postService,
		processors:      processing.NewProcessors(adapter, postService),
		blockScheduler:  scheduler.NewBlockScheduler(postService, client, adapter, cfg.Blockchain.BatchSize),
	}
}

func (l *Listener) Run() {
	currentBlock, err := l.blockScheduler.GetCurrentBlock()
	if err != nil {
		log.Fatal(err)
	}
	headerBlock, err := l.client.HeaderByNumber(l.ctx, nil)
	if err != nil {
		log.Fatal(err)
	}
	for {
		if currentBlock.Cmp(headerBlock.Number) >= 0 {
			headerBlock, _ = l.client.HeaderByNumber(l.ctx, nil)
		}

		if currentBlock.Cmp(headerBlock.Number) == 1 {
			continue
		}

		toBlock := l.blockScheduler.GetToBlock(currentBlock, headerBlock.Number)
		query := ethereum.FilterQuery{
			FromBlock: currentBlock,
			ToBlock:   toBlock,
			Addresses: []common.Address{l.contractAddress},
		}
		logs, err := l.client.FilterLogs(l.ctx, query)
		if err != nil {
			log.Fatal(err)
		}

		err = l.processBlocks(logs, toBlock)

		if err == nil {
			log.Printf("Processed from: %d, to: %d", currentBlock, toBlock)
			currentBlock = currentBlock.Add(toBlock, big.NewInt(1))
		} else {
			log.Fatal(err)
		}
	}
}

func (l *Listener) processBlocks(logs []types.Log, lastBlockNumber *big.Int) error {
	tx, err := l.adapter.BeginTx()
	if err != nil {
		log.Fatal(nil)
	}
	defer func() { _ = (*tx).Rollback(l.ctx) }()

	for _, eventLog := range logs {
		if err = l.processors.Process(eventLog); err != nil {
			return err
		}
	}

	if err = l.adapter.UpdateLastBlock(lastBlockNumber.Int64()); err != nil {
		log.Fatal(err)
		return err
	}

	_ = (*tx).Commit(l.ctx)
	return nil
}
