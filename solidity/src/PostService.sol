// SPDX-License-Identifier: MIT
pragma solidity ^0.8.9;

import {Strings} from "lib/openzeppelin-contracts/contracts/utils/Strings.sol";
import {Initializable} from "lib/openzeppelin-contracts-upgradeable/contracts/proxy/utils/Initializable.sol";

//* @title Twithereum storage service */
contract PostService is Initializable {
    string public constant USER_DOESNT_EXIST_ERROR =
        "Address is not registered: ";
    string public constant USER_ALREADY_EXIST_ERROR =
        "Address is already registered: ";
    string public constant POST_DOESNT_EXIST_ERROR = "Post does not exist: ";
    string public constant POST_NOT_OWNED_ERROR =
        "Caller address don't own the post: ";
    string public constant USERNAME_TAKEN_ERROR = "Username already taken: ";
    string public constant USERNAME_EMPTY_ERROR = "Username cannot be empty";
    string public constant EMPTY_POST_ERROR = "Post can't be empty";
    string public constant ALREADY_FOLLOWED_ERROR = "User already followed";
    string public constant USER_NOT_FOLLOWED_ERROR = "User not followed";

    struct PostInfo {
        uint48 postId;
        address author;
        bytes post;
        uint256 timestamp;
    }

    modifier userExist(address user) {
        require(
            registeredUsers[user],
            string.concat(
                USER_DOESNT_EXIST_ERROR,
                Strings.toHexString(uint160(user), 20)
            )
        );
        _;
    }

    modifier userNotExist(address user) {
        require(
            registeredUsers[user] == false,
            string.concat(
                USER_ALREADY_EXIST_ERROR,
                Strings.toHexString(uint160(user), 20)
            )
        );
        _;
    }

    modifier postExist(uint48 postId) {
        require(
            postIdToIdx[postId] != 0,
            string.concat(POST_DOESNT_EXIST_ERROR, Strings.toString(postId))
        );
        _;
    }

    modifier postOwnedBySender(uint48 postId) {
        require(
            posts[postIdToIdx[postId]].author == msg.sender,
            string.concat(POST_NOT_OWNED_ERROR, Strings.toString(postId))
        );
        _;
    }

    modifier usernameIsValid(string calldata username) {
        require(
            takenUsernames[username] == false,
            string.concat(USERNAME_TAKEN_ERROR, username)
        );
        require(bytes(username).length != 0, USERNAME_EMPTY_ERROR);
        _;
    }

    modifier postNotEmpty(bytes calldata post) {
        require(post.length != 0, EMPTY_POST_ERROR);
        _;
    }

    modifier senderNotFollowed(address follower, address following) {
        require(
            isFollowed(follower, following) == false,
            ALREADY_FOLLOWED_ERROR
        );
        _;
    }

    modifier senderFollowed(address follower, address following) {
        require(
            isFollowed(follower, following) == true,
            USER_NOT_FOLLOWED_ERROR
        );
        _;
    }

    //User data
    address[] public users;
    mapping(address => uint32) public userId;
    mapping(address => bool) public registeredUsers;
    mapping(address => string) public usernames;
    mapping(string => bool) public takenUsernames;

    // Followers data
    mapping(address => uint32[]) public followed;

    // Posts data
    uint48 public totalPost;
    uint48 public postCount;
    PostInfo[] public posts;
    mapping(uint48 => uint48) public postIdToIdx;

    //creation data
    uint256 public initBlock;

    event NewUser(address user, string username, uint256 timestamp);
    event NewPost(uint48 postId, address author, bytes post, uint256 timestamp);
    event UsernameChange(address user, string username, uint256 timestamp);
    event DeletePost(uint48 postId, uint256 timestamp);
    event NewFollow(address follower, address followed, uint256 timestamp);
    event Unfollow(address follower, address unfollowed, uint256 timestamp);

    function initialize() public initializer {
        posts.push(PostInfo(0, address(0), "", block.timestamp));
        ++postCount;
        ++totalPost;
        initBlock = block.number;
    }

    /** @dev Adds address to the system.
     * @notice Required to call any other functions.
     *
     */
    function register(
        string calldata username
    ) external userNotExist(msg.sender) usernameIsValid(username) {
        userId[msg.sender] = uint32(users.length);
        registeredUsers[msg.sender] = true;
        users.push(msg.sender);
        usernames[msg.sender] = username;
        takenUsernames[username] = true;
        emit NewUser(msg.sender, username, block.timestamp);
    }

    /** @dev Adds post to the system.
     */
    function makePost(
        bytes calldata post
    ) external userExist(msg.sender) postNotEmpty(post) {
        uint48 postId = totalPost;
        postIdToIdx[postId] = postCount;
        posts.push(PostInfo(postId, msg.sender, post, block.timestamp));
        ++postCount;
        ++totalPost;
        emit NewPost(postId, msg.sender, post, block.timestamp);
    }

    /** @dev Delete post from the system.
     */
    function deletePost(
        uint48 postId
    )
        external
        userExist(msg.sender)
        postExist(postId)
        postOwnedBySender(postId)
    {
        uint48 idxToRemoveAt = uint48(postIdToIdx[postId]);
        posts[idxToRemoveAt] = posts[posts.length - 1];
        postIdToIdx[posts[posts.length - 1].postId] = idxToRemoveAt;
        posts.pop();
        postIdToIdx[postId] = 0;
        --postCount;
        emit DeletePost(postId, block.timestamp);
    }

    /** @dev Assigns username to the address.
     * @param username Desired username.
     */
    function changeUsername(
        string calldata username
    ) external userExist(msg.sender) usernameIsValid(username) {
        takenUsernames[usernames[msg.sender]] = false;
        takenUsernames[username] = true;
        usernames[msg.sender] = username;
        emit UsernameChange(msg.sender, username, block.timestamp);
    }

    /** @dev Follow to address.
     *  @param following Address to follow.
     */
    function follow(
        address following
    )
        external
        userExist(msg.sender)
        userExist(following)
        senderNotFollowed(msg.sender, following)
    {
        followed[msg.sender].push(userId[following]);
        emit NewFollow(msg.sender, following, block.timestamp);
    }

    /** @dev Unfollow to address.
     *  @param unfollowing Address to unfollow.
     */
    function unfollow(
        address unfollowing
    )
        external
        userExist(msg.sender)
        userExist(unfollowing)
        senderFollowed(msg.sender, unfollowing)
    {
        for (uint i = 0; i < followed[msg.sender].length; ++i) {
            if (users[followed[msg.sender][i]] == unfollowing) {
                uint32 id = followed[msg.sender][
                    followed[msg.sender].length - 1
                ];
                followed[msg.sender].pop();
                if (i < followed[msg.sender].length) {
                    followed[msg.sender][i] = id;
                }
                emit Unfollow(msg.sender, unfollowing, block.timestamp);
                break;
            }
        }
    }

    /** @dev Checks if address is registered.
     * @param user User address.
     * @return bool True if user exists
     */
    function isUserExist(address user) external view returns (bool) {
        return registeredUsers[user];
    }

    function isFollowed(
        address follower,
        address following
    ) internal view returns (bool) {
        for (uint i = 0; i < followed[follower].length; ++i) {
            if (users[followed[msg.sender][i]] == following) {
                return true;
            }
        }
        return false;
    }

    /** @dev Gets all registered users.
     * @return users Registered users.
     */
    function getUsers() external view returns (address[] memory) {
        return users;
    }

    /** @dev Gets addresses followed by user.
     * @return users Addresses followed by user.
     */
    function getFollowed(address user) external view returns (uint32[] memory) {
        return followed[user];
    }

    /** @dev Gets all posts.
     */
    function getPosts() external view returns (PostInfo[] memory) {
        return posts;
    }
}
