// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.13;

import {Ownable} from "../../lib/openzeppelin-contracts/contracts/access/Ownable.sol";
import {TransparentProxy} from "./TransparentProxy.sol";

contract ProxyAdmin is Ownable {
    function getProxyImplementation(
        TransparentProxy proxy
    ) public view virtual returns (address) {
        // We need to manually run the static call since the getter cannot be flagged as view
        (bool success, bytes memory returndata) = address(proxy).staticcall(
            hex"aaf10f42"            // bytes4(keccak256("getImplementation()")) = 0xaaf10f42
        );
        require(success);
        return abi.decode(returndata, (address));
    }

    function getProxyAdmin(
        TransparentProxy proxy
    ) public view virtual returns (address) {
        // We need to manually run the static call since the getter cannot be flagged as view
        (bool success, bytes memory returndata) = address(proxy).staticcall(
            hex"6e9960c3"                     // bytes4(keccak256("getAdmin()")) = 0x6e9960c3
            
        );
        require(success);
        return abi.decode(returndata, (address));
    }

    function changeProxyAdmin(
        TransparentProxy proxy,
        address newAdmin
    ) public virtual onlyOwner {
        proxy.changeAdmin(newAdmin);
    }

    function upgradeImplementation(
        TransparentProxy proxy,
        address implementation
    ) public onlyOwner {
        proxy.upgradeImplementation(implementation);
    }

    function upgradeImplementationAndCall(
        TransparentProxy proxy,
        address implementation,
        bytes memory data
    ) public payable onlyOwner {
        proxy.upgradeImplementationAndCall{value: msg.value}(
            implementation,
            data
        );
    }
}
