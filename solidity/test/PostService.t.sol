// SPDX-License-Identifier: MIT
pragma solidity ^0.8.9;

import {Test, stdStorage, StdStorage} from "forge-std/Test.sol";
import {Strings} from "lib/openzeppelin-contracts/contracts/utils/Strings.sol";
import {PostService} from "src/PostService.sol";
import "forge-std/console.sol";

contract PostServiceBuilder is PostService {
    constructor() {
        super.initialize();
    }

    function build() public view returns (PostService) {
        return PostService(this);
    }

    function withUser(
        address user,
        string calldata username
    ) public returns (PostServiceBuilder) {
        userId[user] = uint32(users.length);
        registeredUsers[user] = true;
        users.push(user);
        usernames[user] = username;
        takenUsernames[username] = true;
        return this;
    }

    function withFollow(
        address user,
        address toFollow
    ) public returns (PostServiceBuilder) {
        followed[user].push(userId[toFollow]);

        return this;
    }

    function withPost(
        address author,
        bytes memory post
    ) public returns (PostServiceBuilder) {
        postIdToIdx[totalPost] = postCount;
        posts.push(PostInfo(totalPost, author, post, block.timestamp));
        ++postCount;
        ++totalPost;

        return this;
    }
}

contract PostServiceTestHelper {
    PostService __p = new PostService();

    function USER_ALREADY_EXIST_ERROR(
        address _address
    ) internal view returns (bytes memory) {
        return __wrap(__p.USER_ALREADY_EXIST_ERROR(), _address);
    }

    function USER_DOESNT_EXIST_ERROR(
        address _address
    ) internal view returns (bytes memory) {
        return __wrap(__p.USER_DOESNT_EXIST_ERROR(), _address);
    }

    function USERNAME_TAKEN_ERROR(
        string memory username
    ) internal view returns (bytes memory) {
        return __wrap(__p.USERNAME_TAKEN_ERROR(), username);
    }

    function USERNAME_EMPTY_ERROR() internal view returns (bytes memory) {
        return __wrap(__p.USERNAME_EMPTY_ERROR());
    }

    function EMPTY_POST_ERROR() internal view returns (bytes memory) {
        return __wrap(__p.EMPTY_POST_ERROR());
    }

    function POST_DOESNT_EXIST_ERROR(
        uint48 postId
    ) internal view returns (bytes memory) {
        return __wrap(__p.POST_DOESNT_EXIST_ERROR(), postId);
    }

    function POST_NOT_OWNED_ERROR(
        uint48 postId
    ) internal view returns (bytes memory) {
        return __wrap(__p.POST_NOT_OWNED_ERROR(), postId);
    }

    function __wrap(string memory m) internal pure returns (bytes memory) {
        return bytes(m);
    }

    function __wrap(
        string memory m1,
        string memory m2
    ) internal pure returns (bytes memory) {
        return bytes(string.concat(m1, m2));
    }

    function __wrap(
        string memory m,
        uint48 value
    ) internal pure returns (bytes memory) {
        return bytes(string.concat(m, Strings.toString(value)));
    }

    function __wrap(
        string memory m,
        address value
    ) internal pure returns (bytes memory) {
        return bytes(string.concat(m, Strings.toHexString(uint160(value), 20)));
    }
}

contract PostServiceTest is Test, PostServiceTestHelper {
    using stdStorage for StdStorage;

    PostServiceBuilder public postServiceBuilder;

    event NewUser(address user, string username, uint256 timestamp);
    event NewPost(uint48 postId, address author, bytes post, uint256 timestamp);
    event UsernameChange(address user, string username, uint256 timestamp);
    event DeletePost(uint48 postId, uint256 timestamp);
    event NewFollow(address follower, address followed, uint256 timestamp);
    event Unfollow(address follower, address unfollowed, uint256 timestamp);

    function setUp() public {
        postServiceBuilder = new PostServiceBuilder();
    }

    function test_postCount_DefaultValue() public {
        PostService postService = postServiceBuilder.build();

        assertEq(postService.postCount(), 1, "Post count is not 1");
    }

    function test_totalPost_DefaultValue() public {
        PostService postService = postServiceBuilder.build();

        assertEq(postService.totalPost(), 1, "Total post is not 1");
    }

    function test_register_NewUser() public {
        PostService postService = postServiceBuilder.build();
        vm.expectEmit(true, true, true, true);
        emit NewUser(address(this), "Ivan", block.timestamp);

        postService.register("Ivan");

        assertEq(
            postService.registeredUsers(address(this)),
            true,
            "User is not registered"
        );
        assertEq(
            postService.usernames(address(this)),
            "Ivan",
            "User registered with wrong username"
        );
        assertEq(
            postService.takenUsernames("Ivan"),
            true,
            "Registered username is not taken"
        );
        assertEq(postService.getUsers().length, 1, "Wrong users count");
        assertEq(postService.users(0), address(this), "Added wrong user");
    }

    function test_register_NewUserUsernameTaken() public {
        PostService postService = postServiceBuilder
            .withUser(address(1), "Ivan")
            .build();

        vm.expectRevert(USERNAME_TAKEN_ERROR("Ivan"));
        postService.register("Ivan");
    }

    function test_register_NewUserUsernameIsEmpty() public {
        PostService postService = postServiceBuilder
            .withUser(address(1), "Ivan")
            .build();

        vm.expectRevert(USERNAME_EMPTY_ERROR());
        postService.register("");
    }

    function test_register_NewUserAlreadyExists() public {
        PostService postService = postServiceBuilder
            .withUser(address(this), "Ivan")
            .build();

        vm.expectRevert(USER_ALREADY_EXIST_ERROR(address(this)));
        postService.register("Ivan");
    }

    function test_makePost_Success() public {
        PostService postService = postServiceBuilder
            .withUser(address(this), "Ivan")
            .build();
        uint48 expectedPostId = 1;
        address expectedAddress = address(this);
        bytes memory expectedPost = "post data";
        uint256 expectedTimestamp = block.timestamp;
        vm.expectEmit(true, true, true, true);
        emit NewPost(expectedPostId, expectedAddress, expectedPost, expectedTimestamp);

        postService.makePost(expectedPost);

        (
            uint48 actualPostId,
            address actualAuthor,
            bytes memory actualPost,
            uint256 actualTimestamp
        ) = postService.posts(expectedPostId);
        assertEq(actualPostId, expectedPostId, "Wrong post ID");
        assertEq(actualAuthor, expectedAddress, "Wrong address");
        assertEq(actualPost, expectedPost, "Wrong post");
        assertEq(actualTimestamp, expectedTimestamp, "Wrong timestamp");
        assertEq(postService.postIdToIdx(expectedPostId), 1, "Wrong post id");
        assertEq(postService.getPosts().length, 2, "Wrong posts length");
        assertEq(postService.postCount(), 2, "Wrong post count");
        assertEq(postService.totalPost(), 2, "Wrong total post count");
    }

    function test_makePost_EmptyPost() public {
        PostService postService = postServiceBuilder
            .withUser(address(this), "Ivan")
            .build();
        vm.expectRevert(EMPTY_POST_ERROR());

        postService.makePost("");
    }

    function test_makePost_NotRegistered() public {
        PostService postService = postServiceBuilder.build();
        vm.expectRevert(USER_DOESNT_EXIST_ERROR(address(this)));

        postService.makePost("123");
    }

    function test_deletePost_Success() public {
        PostService postService = postServiceBuilder
            .withUser(address(this), "Ivan")
            .withUser(address(1), "Ivan1")
            .withPost(address(1), "post data1")
            .withPost(address(this), "post data2")
            .withPost(address(this), "post data3")
            .build();
        uint48 expectedPostId = 2;
        uint48 expectedPostIndex = 2;
        uint48 expectedPostsCount = 3;
        vm.expectEmit(true, true, true, true);
        emit DeletePost(expectedPostId, block.timestamp);

        postService.deletePost(expectedPostId);

        assertEq(
            postService.getPosts().length,
            expectedPostsCount,
            "Wrong posts length"
        );
        assertEq(
            postService.postCount(),
            expectedPostsCount,
            "Wrong post count"
        );
        assertEq(
            postService.totalPost(),
            expectedPostsCount + 1,
            "Wrong total post count"
        );
        assertEq(
            postService.postIdToIdx(expectedPostId),
            0,
            "Deleted postId idx is not 0"
        );
        assertEq(
            postService.postIdToIdx(3),
            expectedPostIndex,
            "Last post is not moved to deleted place"
        );
    }

    function test_deletePost_NotRegistered() public {
        PostService postService = postServiceBuilder
            .withUser(address(this), "Ivan")
            .withUser(address(1), "Ivan1")
            .withPost(address(1), "post data1")
            .withPost(address(this), "post data2")
            .withPost(address(this), "post data3")
            .build();
        address caller = address(10);

        vm.startPrank(caller);
        vm.expectRevert(USER_DOESNT_EXIST_ERROR(caller));
        postService.deletePost(1);
        vm.stopPrank();
    }

    function test_deletePost_NotOwned() public {
        PostService postService = postServiceBuilder
            .withUser(address(this), "Ivan")
            .withUser(address(1), "Ivan1")
            .withPost(address(1), "post data1")
            .withPost(address(this), "post data2")
            .withPost(address(this), "post data3")
            .build();
        uint48 expectedPostId = 1;

        vm.expectRevert(POST_NOT_OWNED_ERROR(expectedPostId));
        postService.deletePost(expectedPostId);
    }

    function test_deletePost_ZeroId() public {
        PostService postService = postServiceBuilder
            .withUser(address(this), "Ivan")
            .withUser(address(1), "Ivan1")
            .withPost(address(1), "post data1")
            .withPost(address(this), "post data2")
            .withPost(address(this), "post data3")
            .build();

        vm.expectRevert(POST_DOESNT_EXIST_ERROR(0));
        postService.deletePost(0);
    }

    function test_deletePost_TooBigId() public {
        PostService postService = postServiceBuilder
            .withUser(address(this), "Ivan")
            .withUser(address(1), "Ivan1")
            .withPost(address(1), "post data1")
            .withPost(address(this), "post data2")
            .withPost(address(this), "post data3")
            .build();

        vm.expectRevert(POST_DOESNT_EXIST_ERROR(10));
        postService.deletePost(10);
    }

    function test_deletePost_AlreadyDeleted() public {
        PostService postService = postServiceBuilder
            .withUser(address(this), "Ivan")
            .withUser(address(1), "Ivan1")
            .withPost(address(1), "post data1")
            .withPost(address(this), "post data2")
            .withPost(address(this), "post data3")
            .build();
        uint48 expectedPostId = 2;
        postService.deletePost(expectedPostId);

        vm.expectRevert(POST_DOESNT_EXIST_ERROR(expectedPostId));
        postService.deletePost(expectedPostId);
    }

    function test_changeUsername_Success() public {
        string memory username = "Ivan";
        string memory newUsername = "Petr";
        PostService postService = postServiceBuilder
            .withUser(address(this), username)
            .build();
        vm.expectEmit(true, true, true, true);
        emit UsernameChange(address(this), newUsername, block.timestamp);

        postService.changeUsername(newUsername);

        assertEq(
            postService.takenUsernames(username),
            false,
            "Previous username is not freed"
        );
        assertEq(
            postService.takenUsernames(newUsername),
            true,
            "New username is not taken"
        );
        assertEq(
            postService.usernames(address(this)),
            newUsername,
            "Username is not updated"
        );
    }

    function test_changeUsername_UsernameAlreadyTaken() public {
        string memory username = "Ivan";
        string memory takenUsername = "Petr";
        PostService postService = postServiceBuilder
            .withUser(address(this), username)
            .withUser(address(1), takenUsername)
            .build();

        vm.expectRevert(USERNAME_TAKEN_ERROR(takenUsername));
        postService.changeUsername(takenUsername);
    }

    function test_changeUsername_UsernameIsEmpty() public {
        PostService postService = postServiceBuilder
            .withUser(address(this), "Ivan")
            .build();

        vm.expectRevert(USERNAME_EMPTY_ERROR());
        postService.changeUsername("");
    }

    function test_changeUsername_NotRegistered() public {
        PostService postService = postServiceBuilder
            .withUser(address(this), "Ivan")
            .withUser(address(1), "Petr")
            .build();
        address caller = address(2);

        vm.startPrank(caller);
        vm.expectRevert(USER_DOESNT_EXIST_ERROR(caller));
        postService.changeUsername("Tigr");
        vm.stopPrank();
    }

    function test_follow_Success() public {
        address caller = address(1);
        address following = address(2);
        PostService postService = postServiceBuilder
            .withUser(caller, "Ivan")
            .withUser(following, "Petr")
            .build();
        vm.expectEmit(true, true, true, true);
        emit NewFollow(caller, following, block.timestamp);

        vm.startPrank(caller);
        postService.follow(following);
        vm.stopPrank();

        assertEq(
            postService.users(postService.followed(caller, 0)),
            following,
            "User did not follow"
        );
    }

    function test_follow_NotRegistered() public {
        address caller = address(this);
        address following = address(2);
        PostService postService = postServiceBuilder
            .withUser(address(1), "Ivan")
            .withUser(following, "Petr")
            .build();

        vm.expectRevert(USER_DOESNT_EXIST_ERROR(caller));
        postService.follow(following);
    }

    function test_follow_FollowingNotRegistered() public {
        address caller = address(1);
        address following = address(2);
        PostService postService = postServiceBuilder
            .withUser(caller, "Ivan")
            .withUser(address(3), "Petr")
            .build();

        vm.startPrank(caller);
        vm.expectRevert(USER_DOESNT_EXIST_ERROR(following));
        postService.follow(following);
        vm.stopPrank();
    }

    function test_unfollow_Success() public {
        address caller = address(1);
        address unfollowing = address(2);
        uint256 expectedLength = 2;
        PostService postService = postServiceBuilder
            .withUser(caller, "Ivan")
            .withUser(unfollowing, "Petr")
            .withUser(address(3), "Petr")
            .withUser(address(4), "Petr")
            .withFollow(caller, unfollowing)
            .withFollow(caller, address(3))
            .withFollow(caller, address(4))
            .build();
        vm.expectEmit(true, true, true, true);
        emit Unfollow(caller, unfollowing, block.timestamp);

        vm.startPrank(caller);
        postService.unfollow(unfollowing);
        vm.stopPrank();

        uint32[] memory following = postService.getFollowed(caller);
        for (uint i = 0; i < following.length; ++i) {
            assertTrue(
                postService.users(following[i]) != unfollowing,
                "Address in followed"
            );
        }
        assertEq(
            postService.getFollowed(caller).length,
            expectedLength,
            "Followers length not changed"
        );
    }

    function test_unfollow_NotRegistered() public {
        address caller = address(this);
        address unfollowing = address(2);
        PostService postService = postServiceBuilder
            .withUser(unfollowing, "Petr")
            .withUser(address(3), "Petr")
            .withUser(address(4), "Petr")
            .build();

        vm.expectRevert(USER_DOESNT_EXIST_ERROR(caller));
        postService.unfollow(unfollowing);
    }

    function test_unfollow_FollowingNotRegistered() public {
        address caller = address(1);
        address unfollowing = address(2);
        PostService postService = postServiceBuilder
            .withUser(caller, "Ivan")
            .withUser(address(3), "Petr")
            .withUser(address(4), "Petr")
            .withFollow(caller, address(3))
            .withFollow(caller, address(4))
            .build();

        vm.startPrank(caller);
        vm.expectRevert(USER_DOESNT_EXIST_ERROR(unfollowing));
        postService.unfollow(unfollowing);
        vm.stopPrank();
    }
}
