// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.13;

import "forge-std/Script.sol";
import {PostService} from "src/PostService.sol";
import {ProxyAdmin} from "src/proxy/ProxyAdmin.sol";
import {TransparentProxy} from "src/proxy/TransparentProxy.sol";
import "forge-std/console.sol";

contract DeployV0Script is Script {
    function setUp() public {}

    function run() public {
        uint256 deployerPrivateKey = vm.envUint("PRIVATE_KEY");

        vm.startBroadcast(deployerPrivateKey);
        PostService postService = new PostService();
        ProxyAdmin proxyAdmin = new ProxyAdmin();
        TransparentProxy transparentProxy = new TransparentProxy(address(postService), address(proxyAdmin), "");
        console.log("postServiceAddress: ", address(postService));
        console.log("proxyAdminAddress: ", address(proxyAdmin));
        console.log("transparentProxyAddress: ", address(transparentProxy));
        vm.stopBroadcast();
    }
}
