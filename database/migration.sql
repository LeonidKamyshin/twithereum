CREATE SCHEMA IF NOT EXISTS tw;

CREATE TABLE IF NOT EXISTS tw.users(
    id bigserial,
    address varchar(512) PRIMARY KEY,
    username varchar(512) UNIQUE,
    followers bigint DEFAULT 0,
    followed bigint DEFAULT 0
);

CREATE TABLE IF NOT EXISTS tw.posts(
    id bigint PRIMARY KEY,
    address varchar(512) NOT NULL REFERENCES tw.users,
    p_data bytea NOT NULL,
    created_at timestamptz NOT NULL
);

CREATE TABLE IF NOT EXISTS tw.followers(
    id bigserial,
    user_id varchar(512) NOT NULL REFERENCES tw.users,
    followed_id varchar(512) NOT NULL REFERENCES tw.users,
    PRIMARY KEY(user_id, followed_id)
);

CREATE TABLE IF NOT EXISTS tw.blockchain(
    one_row bool PRIMARY KEY DEFAULT TRUE,
    lastBlock bigint DEFAULT 0,
    CONSTRAINT one_row_constraint CHECK (one_row)
);

CREATE FUNCTION f_new_follower()
    RETURNS TRIGGER
    LANGUAGE PLPGSQL
AS $$
BEGIN
    UPDATE tw.users SET followers=followers+1 WHERE address=NEW.followed_id;
    UPDATE tw.users SET followed=followed+1 WHERE address=NEW.user_id;
    RETURN NEW;
END;
$$;

CREATE TRIGGER t_new_follower
    AFTER INSERT ON tw.followers
    FOR EACH ROW
EXECUTE FUNCTION f_new_follower();

CREATE FUNCTION f_unfollow()
    RETURNS TRIGGER
    LANGUAGE PLPGSQL
AS $$
BEGIN
    UPDATE tw.users SET followers=followers-1 WHERE address=OLD.followed_id;
    UPDATE tw.users SET followed=followed-1 WHERE address=OLD.user_id;
    RETURN OLD;
END;
$$;

CREATE TRIGGER t_unfollow
    AFTER DELETE ON tw.followers
    FOR EACH ROW
EXECUTE FUNCTION f_unfollow();

INSERT INTO tw.blockchain (lastblock) VALUES (0);
